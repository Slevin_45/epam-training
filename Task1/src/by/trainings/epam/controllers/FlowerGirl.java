package by.trainings.epam.controllers;

import by.trainings.epam.beans.Bouquet;
import by.trainings.epam.beans.Flower;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 21:38
 * To change this template use File | Settings | File Templates.
 */
public class FlowerGirl {
    private final static Logger logger = Logger.getLogger(FlowerGirl.class);
    private Bouquet bouquet;
    private ArrayList<Flower> bouquetClone;

    public void addBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public void addFlowerToBouquet(Flower flower) {
        this.bouquet.add(flower);
    }

    public void removeFlowerFromBouquet(Flower flower) {
        this.bouquet.remove(flower);
    }

    public void initClone() {  // call it in main() for initialization of bouquetClone. If I will use getClone() clone will change with every calling.
        bouquetClone = bouquet.clone();
    }

    public ArrayList<Flower> getBouquetClone() {
        return bouquetClone;
    }

    public StringBuilder getInfoAboutBouquet() {
        StringBuilder infoAboutBouquet = new StringBuilder();
        int cost = 0;
        for (Flower flower : bouquetClone) {
            logger.info(flower);
            infoAboutBouquet.append(flower);
            cost = cost + flower.getCost();
        }
        logger.info("Cost of a bouquet :" + cost);
        return infoAboutBouquet;
    }

    public ArrayList<Flower> sortByFreshLevel() {
        Collections.sort(bouquetClone, new SorterByFreshLevel());
        logger.info("Sorting has done");
        return bouquetClone;
    }

    public Flower findFlowerInRangeOfStalkLength(int leftLimit, int rightLimit) {
        int index = 0;
        for (Flower flower : bouquetClone) {
            if (flower.getLengthOfStalk() >= leftLimit && flower.getLengthOfStalk() <= rightLimit) {
                logger.info("Flower in this range : " + flower);
                index = bouquetClone.indexOf(flower);
                return bouquetClone.get(index);
            }
        }
        return null;
    }

}
