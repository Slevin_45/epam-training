package by.trainings.epam.controllers;

import by.trainings.epam.beans.Flower;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 14.10.15
 * Time: 23:04
 * To change this template use File | Settings | File Templates.
 */
public class SorterByFreshLevel implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getFreshLevel().getLevelWord().compareTo(o2.getFreshLevel().getLevelWord());

    }
}
