package by.trainings.epam;

import by.trainings.epam.beans.*;
import by.trainings.epam.controllers.FlowerGirl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 20:29
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }

    private final static Logger logger = Logger.getLogger(Main.class);
    private static Flower simpleFlower;
    private static Flower flowerWithThorns;
    private static Flower curlyFlower;
    private static Bouquet bouquet;
    private static FlowerGirl flowerGirl;

    public static void main(String[] args) {
        stubData();
        addToBouquet();
        operationsWithBouquet();
    }

    private static void stubData() {
        bouquet = new Bouquet();
        flowerGirl = new FlowerGirl();
        flowerGirl.addBouquet(bouquet);

        simpleFlower = new SimpleFlower(10, FreshLevel.NOTFRESH, 4);
        flowerWithThorns = new FlowerWithThorns(8, FreshLevel.FRESH, 7, 10);
        curlyFlower = new CurlyFlower(3, FreshLevel.MEDIUM, 10, 7);

        logger.info("Parameters for object are all correct");

    }

    private static void addToBouquet() {
        flowerGirl.addFlowerToBouquet(curlyFlower);
        flowerGirl.addFlowerToBouquet(simpleFlower);
        flowerGirl.addFlowerToBouquet(flowerWithThorns);
        flowerGirl.initClone();
    }

    private static void operationsWithBouquet() {
        flowerGirl.getInfoAboutBouquet();
        flowerGirl.sortByFreshLevel();
        flowerGirl.getInfoAboutBouquet();
        flowerGirl.findFlowerInRangeOfStalkLength(3, 6);
    }
}
