package by.trainings.epam.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public abstract class Flower {
    protected FreshLevel freshLevel;
    protected int stalkLength;
    protected int cost;

    abstract public int getCost();
    abstract public void setCost(int cost);
    abstract public int getLengthOfStalk();
    abstract public FreshLevel getFreshLevel();
    abstract public void setStalkLength(int length);
    abstract public void setFreshLevel(FreshLevel level);
    abstract public String toString();
}
