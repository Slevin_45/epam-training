package by.trainings.epam.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
public class FlowerWithThorns extends Flower {
    private int lengthOfThorns;

    public FlowerWithThorns(int stalkLength, FreshLevel freshLevel1, int cost, int lengthOfThorns) {
        this.freshLevel = freshLevel1;
        this.stalkLength = stalkLength;
        this.cost = cost;
        this.lengthOfThorns = lengthOfThorns;
    }

    public FlowerWithThorns() {
        this.stalkLength = 0;
        this.cost = 0;
        this.lengthOfThorns = 0;
    }

    public int getLengthOfThorns() {
        return lengthOfThorns;
    }

    public void setLengthOfThorns(int lengthOfThorns) {
        this.lengthOfThorns = lengthOfThorns;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public int getLengthOfStalk() {
        return stalkLength;
    }

    @Override
    public FreshLevel getFreshLevel() {
        return freshLevel;
    }

    @Override
    public void setStalkLength(int length) {
        this.stalkLength = length;
    }

    @Override
    public void setFreshLevel(FreshLevel level) {
        this.freshLevel = level;
    }

    @Override
    public String toString() {
        return "Flower With Thorns : " + "Length of stalk " + stalkLength + ", Cost " + cost + ", Fresh Level  "
                + freshLevel.getLevelWord() + ", Length of thorns " + lengthOfThorns + "\n";
    }

}
