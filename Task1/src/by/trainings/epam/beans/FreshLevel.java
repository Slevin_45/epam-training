package by.trainings.epam.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 20:32
 * To change this template use File | Settings | File Templates.
 */
public enum FreshLevel {
    FRESH("fresh"),
    MEDIUM("medium"),
    NOTFRESH("not fresh");

    private String levelWord;

    FreshLevel(String levelWord) {
        this.levelWord = levelWord;
    }

    public String getLevelWord() {
        return levelWord;
    }
}
