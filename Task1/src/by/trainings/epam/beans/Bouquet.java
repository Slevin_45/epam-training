package by.trainings.epam.beans;

import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 21:20
 * To change this template use File | Settings | File Templates.
 */
public class Bouquet implements Cloneable {

    private final static Logger logger = Logger.getLogger(Bouquet.class);

    private ArrayList<Flower> bouquet;

    public Bouquet() {
        bouquet = new ArrayList<>();
    }

    public void add(Flower flower) {
        bouquet.add(flower);
    }

    public void remove(Flower flower) {
        bouquet.remove(flower);
    }

    @Override
    public ArrayList<Flower> clone() {
        Bouquet cloned = null;
        try {
            cloned = (Bouquet) super.clone();
            cloned.bouquet = (ArrayList<Flower>) bouquet.clone();
        } catch (CloneNotSupportedException e) {
            logger.error("Wrong cloning", e);
        }
        return cloned.bouquet;
    }
}
