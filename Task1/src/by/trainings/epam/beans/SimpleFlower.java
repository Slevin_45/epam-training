package by.trainings.epam.beans;

public class SimpleFlower extends Flower  {

    public SimpleFlower(int stalkLength, FreshLevel freshLevel1, int cost) {
        this.stalkLength = stalkLength;
        this.freshLevel = freshLevel1;
        this.cost = cost;
    }

    public SimpleFlower() {
        this.stalkLength = 0;
        this.cost = 0;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getLengthOfStalk() {
        return stalkLength;
    }

    public FreshLevel getFreshLevel() {
        return freshLevel;
    }

    public void setStalkLength(int length) {
        this.stalkLength = length;
    }

    public void setFreshLevel(FreshLevel level) {
        this.freshLevel = level;
    }

    @Override
    public String toString() {
        return "SimpleFlower : " + "Length of stalk " + stalkLength + ", Fresh Level  " + freshLevel.getLevelWord()
                + ", Cost " + cost + "\n";
    }

}
