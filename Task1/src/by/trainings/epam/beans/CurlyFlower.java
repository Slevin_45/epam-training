package by.trainings.epam.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 13.10.15
 * Time: 20:49
 * To change this template use File | Settings | File Templates.
 */
public class CurlyFlower extends Flower {
    private int twirlRadius;

    public CurlyFlower(int stalkLength, FreshLevel freshLevel1, int cost, int twirlRadius) {
        this.freshLevel = freshLevel1;
        this.stalkLength = stalkLength;
        this.cost = cost;
        this.twirlRadius = twirlRadius;
    }

    public CurlyFlower() {
        this.stalkLength = 0;
        this.cost = 0;
        this.twirlRadius = 0;
    }

    public void setTwirlRadius(int radius) {
        twirlRadius = radius;
    }

    public int getTwirlRadius() {
        return twirlRadius;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public int getLengthOfStalk() {
        return stalkLength;
    }

    @Override
    public FreshLevel getFreshLevel() {
        return freshLevel;
    }

    @Override
    public void setStalkLength(int length) {
        this.stalkLength = length;
    }

    @Override
    public void setFreshLevel(FreshLevel level) {
        this.freshLevel = level;
    }

    @Override
    public String toString() {
        return "CurlyFlower : " + "Length of stalk " + stalkLength + ", Fresh Level  " + freshLevel.getLevelWord()
                + ", Cost " + cost + ", Twirl radius " + twirlRadius + "\n";
    }

}
