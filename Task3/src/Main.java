/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */

import beans.BusStation;
import beans.Passengers;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import threads.Bus;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    private final static Logger logger = Logger.getLogger(Main.class);
    private static ArrayList<BusStation> busStationsForTheFirstBus;
    private static ArrayList<BusStation> busStationsForTheSecondBus;
    private static ArrayList<BusStation> busStationsForTheThirdBus;
    private static ArrayList<BusStation> busStationsForTheFourthBus;
    private static Passengers passengers1;
    private static Passengers passengers2;
    private static Passengers passengers3;
    private static Passengers passengers4;
    private static Exchanger exchanger = new Exchanger();

    static {
        new DOMConfigurator().doConfigure("Log4j.xml", LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        stubData();
        startThreads();
    }

    private static void startThreads(){
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(new Bus(busStationsForTheFirstBus, 1, passengers1, exchanger));
        executor.execute(new Bus(busStationsForTheSecondBus, 2, passengers2, exchanger));
        executor.execute(new Bus(busStationsForTheThirdBus, 3, passengers3, exchanger));
        executor.execute(new Bus(busStationsForTheFourthBus, 4, passengers4, exchanger));
        logger.info("All threads were successfully started");

        executor.shutdown();
        logger.info("Executor finished starting threads");
    }
    private static void stubData() {
        passengers1 = new Passengers(10);
        passengers2 = new Passengers(17);
        passengers3 = new Passengers(15);
        passengers4 = new Passengers(20);

        BusStation aldgate = new BusStation("Aldgate");
        BusStation beckton = new BusStation("Beckton");
        BusStation chingford = new BusStation("Chingford");
        BusStation edgware = new BusStation("Edgware");
        BusStation harrow = new BusStation("Harrow");
        BusStation orpington = new BusStation("Orpington");
        BusStation stratford = new BusStation("Stratford");
        BusStation victoria = new BusStation("Victoria");
        BusStation canning_town = new BusStation("Canning Town");

        busStationsForTheFirstBus = new ArrayList<>();

        busStationsForTheFirstBus.add(aldgate);
        busStationsForTheFirstBus.add(beckton);
        busStationsForTheFirstBus.add(canning_town);
        busStationsForTheFirstBus.add(chingford);

        busStationsForTheSecondBus = new ArrayList<>();

        busStationsForTheSecondBus.add(aldgate);
        busStationsForTheSecondBus.add(beckton);
        busStationsForTheSecondBus.add(canning_town);
        busStationsForTheSecondBus.add(chingford);

        busStationsForTheThirdBus = new ArrayList<>();

        busStationsForTheThirdBus.add(aldgate);
        busStationsForTheThirdBus.add(beckton);
        busStationsForTheThirdBus.add(canning_town);
        busStationsForTheThirdBus.add(chingford);

        busStationsForTheFourthBus = new ArrayList<>();

        busStationsForTheFourthBus.add(canning_town);
        busStationsForTheFourthBus.add(harrow);
        busStationsForTheFourthBus.add(beckton);
        busStationsForTheFourthBus.add(chingford);
    }
}
