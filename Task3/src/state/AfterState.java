package state;

import beans.BusStation;
import org.apache.log4j.Logger;
import threads.Bus;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public class AfterState implements IState {
    private final static Logger logger = Logger.getLogger(AfterState.class);

    @Override
    public void doAction(Bus bus, BusStation busStation) {
        logger.info("Bus number " + bus.getNumber() + " has recently left station : " + busStation.getName()
                + " with " + bus.getPassengers().getQuantity() + " passengers");
    }

    public String toString() {
        return "After state";
    }
}
