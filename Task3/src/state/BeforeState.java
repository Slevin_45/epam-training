package state;

import beans.BusStation;
import org.apache.log4j.Logger;
import threads.Bus;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
public class BeforeState implements IState {
    private final static Logger logger = Logger.getLogger(BeforeState.class);

    @Override
    public void doAction(Bus bus, BusStation busStation) {
        logger.info("Bus number " + bus.getNumber() + "is going to the station " + busStation.getName());
    }

    public String toString() {
        return "Before state";
    }
}
