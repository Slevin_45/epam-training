package state;

import beans.BusStation;
import threads.Bus;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
public interface IState {
    void doAction(Bus bus, BusStation busStation);
}
