package state;

import beans.BusStation;
import beans.Passengers;
import org.apache.log4j.Logger;
import threads.Bus;

import java.util.concurrent.Exchanger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 18:02
 * To change this template use File | Settings | File Templates.
 */
public class InState implements IState {
    private final static Logger logger = Logger.getLogger(InState.class);
    private static Exchanger<Passengers> exchanger = new Exchanger<>();
    private Lock lock = new ReentrantLock();

    @Override
    public void doAction(Bus bus, BusStation busStation) {
        logger.info("Bus number " + bus.getNumber() + " is on the station " + busStation.getName());
    }

    public String toString() {
        return "In state";
    }
}
