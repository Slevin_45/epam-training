package threads;

import beans.BusStation;
import beans.Passengers;
import org.apache.log4j.Logger;
import state.AfterState;
import state.BeforeState;

import java.util.ArrayList;
import java.util.concurrent.Exchanger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
public class Bus implements Runnable {
    private final static Logger logger = Logger.getLogger(BusStation.class);
    private Lock lock = new ReentrantLock();
    private Exchanger<Passengers> exchanger;
    public Passengers passengers;
    private ArrayList<BusStation> busStations;
    private int number;
    private BeforeState beforeState = new BeforeState();
    private AfterState afterState = new AfterState();
    private int counter = 3;

    public Bus(ArrayList busStations, int number, Passengers passengers, Exchanger<Passengers> exchanger) {
        this.busStations = busStations;
        this.number = number;
        this.passengers = passengers;
        this.exchanger = exchanger;
    }

    public void addExchanger(Exchanger exchanger) {
        this.exchanger = exchanger;
    }

    public Exchanger getExchanger() {
        return exchanger;
    }

    public void run() {
        for (int i = 0; i < busStations.size(); i++) {
            beforeState.doAction(this, busStations.get(i));
            busStations.get(i).addBus(this);
            exchange(this, busStations.get(i));
            afterState.doAction(this, busStations.get(i));
            busStations.get(i).getSemaphore().release();
        }
    }

    public Bus exchange(Bus bus, BusStation busStation) {
        counter++;
        if (busStation.getSemaphore().availablePermits() == 0) {
            if (counter > 3 && counter < 15) {
                System.out.println(counter + busStation.getName());
                try {
                    this.passengers = exchanger.exchange(passengers);
                } catch (InterruptedException e) {
                    logger.info("Exchanger mistake ", e);
                }
            } else
                bus.setPassengers(new Passengers((int) bus.getPassengers().getQuantity() - counter / 2));
        }
        return bus;
    }

    public void leaveBus(Bus bus) {
        lock.lock();
        bus.setPassengers(new Passengers(4));
        lock.unlock();
    }

    public int getNumber() {
        return number;
    }

    public void setPassengers(Passengers passengers) {
        lock.lock();
        if (passengers.getQuantity() < 0)
            passengers = new Passengers(0);
        try {
            this.passengers = passengers;
        } finally {
            lock.unlock();
        }
    }

    public Passengers getPassengers() {
        lock.lock();
        try {
            return passengers;
        } finally {
            lock.unlock();
        }
    }
}
