package beans;

import org.apache.log4j.Logger;
import state.InState;
import threads.Bus;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 11.11.15
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
public class BusStation {
    private final static Logger logger = Logger.getLogger(BusStation.class);
    private String name;
    private final int busLimit = 2;
    private Semaphore semaphore = new Semaphore(busLimit);
    private Lock lock = new ReentrantLock();
    private InState inState = new InState();

    public BusStation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addBus(Bus bus) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            logger.error("Semaphore error", e);
        }
        inState.doAction(bus, this);
    }

    public Semaphore getSemaphore() {
        lock.lock();
        try {
            return semaphore;
        } finally {
            lock.unlock();
        }
    }
}
