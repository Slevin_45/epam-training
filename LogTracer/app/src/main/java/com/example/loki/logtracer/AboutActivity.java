package com.example.loki.logtracer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Loki on 01.04.2016.
 */
public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
