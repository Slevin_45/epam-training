package tests;

import compositePattern.CompositePart;
import controllers.ModifierOfText;
import junit.framework.Assert;
import org.junit.BeforeClass;
import parser.TextParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 02.11.15
 * Time: 22:35
 * To change this template use File | Settings | File Templates.
 */
public class ModifierOfTextTest {
    private static ModifierOfText modifierOfText;

    @BeforeClass
    public static void init(){
        String path = "files\\info.txt";
        TextParser textParser = new TextParser();
        CompositePart wholeText = textParser.parse(path);
        modifierOfText = new ModifierOfText(wholeText);
    }
    @org.junit.Test
    public void testFindUniqueWordInFirstSentencesThrowTheText() throws Exception {
        String str = modifierOfText.findUniqueWordInFirstSentencesThrowTheText();
        Assert.assertEquals("","hello ",str);
    }

    @org.junit.Test
    public void testSorterWordsFromTheFile() throws Exception {
        ArrayList<String> list = new ArrayList<>();
        list.add("Eldar");
        list.add("hello");
        list.add("statement");
        list.add("the");
        List keyList = new ArrayList(modifierOfText.sorterWordsFromTheFile().values());
        Assert.assertEquals("",list,keyList);
    }

    @org.junit.Test
    public void testChangeWordFromBeginningToEnd() throws Exception {
        String expectedStr = "statements if-then statement is the most basic of all the control flow hello The . ";
        String actualStr = modifierOfText.changeWordFromBeginningToEnd().getElement(0).getElement(0).toString();
        Assert.assertEquals("", expectedStr, actualStr);
    }
}
