package compositePattern;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 22:07
 * To change this template use File | Settings | File Templates.
 */
public class CompositePart implements IPart {
    private ArrayList<IPart> text = new ArrayList<>();

    public CompositePart(CompositePart clone){ //copy constructor
        this.text = new ArrayList<>(clone.text);
    }
    public CompositePart(){

    }
    @Override
    public void addElement(IPart part) {
        text.add(part);
    }

    @Override
    public void removeElement(IPart part) {
        text.remove(part);
    }

    @Override
    public IPart getElement(int index) {
        return text.get(index);
    }
    public int getSize(){
        return text.size();
    }
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (IPart component : text) {
            result.append(component.toString());
        }
        return result.toString();
    }

    @Override
    public void set(int index, IPart part) {
        text.set(index, part);
    }
}
