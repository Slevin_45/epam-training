package compositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
public class LeafPart implements IPart {

    private String str;

    public LeafPart(String str) {
        this.str = str;
    }

    @Override
    public void addElement(IPart component) {
        new UnsupportedOperationException();
    }

    @Override
    public void removeElement(IPart component) {
        new UnsupportedOperationException();
    }

    @Override
    public IPart getElement(int index) {
        return this;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public String toString() {
        return str;
    }

    @Override
    public void set(int index, IPart part) {
        new UnsupportedOperationException();
    }

}
