package compositePattern;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 21:56
 * To change this template use File | Settings | File Templates.
 */
public interface IPart {
    void addElement(IPart component);
    void removeElement(IPart component);
    IPart getElement(int index);
    int getSize();
    String toString();
    void set(int index, IPart part);
}
