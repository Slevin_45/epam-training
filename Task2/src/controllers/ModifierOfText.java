package controllers;

import compositePattern.CompositePart;
import compositePattern.LeafPart;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 23:37
 * To change this template use File | Settings | File Templates.
 */
public class ModifierOfText {
    private final static Logger logger = Logger.getLogger(ModifierOfText.class);

    private CompositePart wholeText;

    public ModifierOfText(CompositePart wholeText) {
        this.wholeText = wholeText;
    }

    public String findUniqueWordInFirstSentencesThrowTheText() {
        int counter;
        int firstParagraph = 0;
        int firstSentencesSize = wholeText.getElement(firstParagraph).getElement(0).getSize();
        int wholeTextSize = wholeText.getSize();
        for (int i = 0; i < firstSentencesSize; i++) {
            counter = 0;
            String word = wholeText.getElement(firstParagraph).getElement(0).getElement(i).toString();
            for (int j = firstParagraph; j < wholeTextSize; j++) {
                String paragraph = wholeText.getElement(j).toString();
                if (paragraph.contains(word))
                    counter++;
            }
            if (counter == 1) {//will be at least one match, because we`r looking for it from the same paragraph
                logger.info("Unique word was successfully found : " + word);
                return word;
            }
        }
        logger.info("Unique word was`t found");
        return null;
    }

    public TreeMap<Integer, String> sorterWordsFromTheFile() {
        TreeMap<Integer, String> sortedWordMap = new TreeMap<>();
        int counter = 0;
        String wordList[] = initialization("files\\wordList.txt").split("\\s+");
        for (int i = 0; i < wordList.length; i++) {
            counter = 0;
            for (int j = 0; j < wholeText.getSize(); j++) {
                if (wholeText.getElement(j).toString().contains(wordList[i])) {
                    counter++;
                }
            }
            sortedWordMap.put(counter, wordList[i]);
        }
        logger.info("Sorted all words from the file");
        return sortedWordMap;
    }

    public CompositePart changeWordFromBeginningToEnd() {
        CompositePart wholeTextClone = new CompositePart(wholeText);
        for (int i = 0; i < wholeTextClone.getSize(); i++) {
            for (int j = 0; j < wholeTextClone.getElement(i).getSize(); j++) {
                int size = wholeTextClone.getElement(i).getElement(j).getSize();
                int first = 0;
                int last = wholeTextClone.getElement(i).getElement(j).getSize() - 1;
                LeafPart firstWord = new LeafPart(wholeTextClone.getElement(i).getElement(j).getElement(first).toString().concat(". "));
                LeafPart lastWord = new LeafPart(wholeTextClone.getElement(i).getElement(j).getElement(last).toString().replace(".", " "));
                wholeTextClone.getElement(i).getElement(j).set(first, lastWord);
                wholeTextClone.getElement(i).getElement(j).set(last, firstWord);
            }
        }
        logger.info("Changed first and last words");
        return wholeTextClone;
    }


    private String initialization(final String path) {
        String text = "";
        try {
            FileInputStream inFile = new FileInputStream(path);
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            text = new String(str);
        } catch (FileNotFoundException e) {
            logger.error("File wasn't found", e);
        } catch (IOException e) {
            logger.error("Wrong output", e);
        }
        return text;
    }

}
