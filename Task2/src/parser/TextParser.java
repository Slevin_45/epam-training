package parser;

import compositePattern.CompositePart;
import compositePattern.LeafPart;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {
    private final static Logger logger = Logger.getLogger(TextParser.class);
    private static final String REGEX_LISTING = "\\s*(Start listing)([^\\t]+)(End listing)";
    private static final String REGEX_PARAGRAPH_WITH_LISTING = "(\\s*(.+))([^(\\s*(Start listing)([^\\t]+)(End listing)\\s)])|\\s*(Start listing)([^\\t]+)(End listing)";
    private static final String REGEX_SENTENCE = "([\\s]*)([^(\\.|\\!|\\?)]*)(\\.|\\!|\\?|\\:)";
    private static final String REGEX_WORD = "([^(\\s)]+)(\\s)*";

    public CompositePart parse(String path) {
        String text = initialization(path);
        CompositePart wholeText = new CompositePart();
        wholeText = parseToParagraph(wholeText, text);
        return wholeText;
    }

    private String initialization(final String path) {
        String text = null;
        try {
            FileInputStream inFile = new FileInputStream(path);
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            text = new String(str);
        } catch (FileNotFoundException e) {
            logger.error("File wasn't found", e);
        } catch (IOException e) {
            logger.error("Wrong output", e);
        }
        return text;
    }

    private CompositePart parseToParagraph(CompositePart wholeText, String text) {
        // parse to paragraph
        CompositePart paragraphList = new CompositePart();
        Pattern patternParagraph = Pattern
                .compile(REGEX_PARAGRAPH_WITH_LISTING);
        LeafPart paragraphLeaf = null;
        String paragraph = null;
        Matcher matcher = patternParagraph.matcher(text);
        while (matcher.find()) {
            paragraph = matcher.group();
            if (Pattern.matches(REGEX_LISTING, paragraph)) {
                // if listing - add to list without parser
                paragraphLeaf = new LeafPart(paragraph);
                paragraphList.addElement(paragraphLeaf);
            } else {
                paragraphList = parseToSentense(paragraphList, paragraph);
                wholeText.addElement(paragraphList);
                paragraphList = new CompositePart();
            }

        }

        return wholeText;
    }

    private CompositePart parseToSentense(CompositePart paragraphList,
                                          String paragraph) {
        // parse to sentence
        CompositePart sentenceList = new CompositePart();
        Pattern patternSentence = Pattern.compile(REGEX_SENTENCE);
        Matcher m2 = patternSentence.matcher(paragraph);
        String sentence = "";
        while (m2.find()) {
            sentence = m2.group();
            sentenceList = parseToWord(sentenceList, sentence);
            paragraphList.addElement(sentenceList);
            sentenceList = new CompositePart();
        }
        return paragraphList;
    }

    private CompositePart parseToWord(CompositePart sentenceList,
                                      String sentence) {
        // parse to word
        Pattern patternWord = Pattern.compile(REGEX_WORD);
        String word = null;
        LeafPart wordList;
        Matcher matcher = patternWord.matcher(sentence);
        while (matcher.find()) {
            word = matcher.group();
            wordList = new LeafPart(word);
            sentenceList.addElement(wordList);
        }
        return sentenceList;
    }
}
