import compositePattern.CompositePart;
import controllers.ModifierOfText;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import parser.TextParser;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 30.10.15
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    private final static Logger logger = Logger.getLogger(Main.class);
    static {
        new DOMConfigurator().doConfigure("Log4j.xml", LogManager.getLoggerRepository());
    }
    public static void main(String[] args) {

        String path = "files\\info.txt";
        TextParser textParser = new TextParser();
        CompositePart wholeText = textParser.parse(path);
        ModifierOfText modifierOfText = new ModifierOfText(wholeText);
        modifierOfText.findUniqueWordInFirstSentencesThrowTheText();
        modifierOfText.sorterWordsFromTheFile();
        modifierOfText.changeWordFromBeginningToEnd();
    }
}
