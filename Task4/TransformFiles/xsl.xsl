<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml"/>
    <xsl:template match="/">
        <devices>
      <xsl:apply-templates/>
        </devices>
    </xsl:template>
    <xsl:template match="computerDevice">
        <computerDevice id="{@id}">
            <name>
                <xsl:value-of select="name"/>
            </name>
            <type peripheral="{type/peripheral}" gpoup="{type/group}" cooler="{type/cooler}">
                <price price="{price}"/>
            </type>
        </computerDevice>
    </xsl:template>
</xsl:stylesheet>