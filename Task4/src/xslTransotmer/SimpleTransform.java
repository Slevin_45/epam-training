package xslTransotmer;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class SimpleTransform {
    public static void main(String[] args) {
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer(new StreamSource("TransformFiles/xsl.xsl"));
            transformer.transform(new StreamSource("src/xml/device.xml"), new StreamResult("TransformFiles/newXml.xml"));
        } catch (TransformerException e) {
            System.err.println("Impossible transform file!" + e);
        }
    }
}