
package jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="computerDevice" maxOccurs="unbounded" minOccurs="3">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="origin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="price">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;minInclusive value="0"/>
 *                         &lt;maxInclusive value="1000"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="type">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="peripheral">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;pattern value="Yes|No|yes|no"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="energy_consumption" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="cooler" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="port">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="USB"/>
 *                                   &lt;enumeration value="HDMI"/>
 *                                   &lt;enumeration value="COM"/>
 *                                   &lt;enumeration value="LPT"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="critical" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "computerDevice"
})
@XmlRootElement(name = "devices")
public class Devices {

    @XmlElement(required = true)
    protected List<Devices.ComputerDevice> computerDevice;

    /**
     * Gets the value of the computerDevice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the computerDevice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComputerDevice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Devices.ComputerDevice }
     * 
     * 
     */
    public List<Devices.ComputerDevice> getComputerDevice() {
        if (computerDevice == null) {
            computerDevice = new ArrayList<Devices.ComputerDevice>();
        }
        return this.computerDevice;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="origin" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="price">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;minInclusive value="0"/>
     *               &lt;maxInclusive value="1000"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="type">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="peripheral">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;pattern value="Yes|No|yes|no"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="energy_consumption" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="cooler" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="port">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="USB"/>
     *                         &lt;enumeration value="HDMI"/>
     *                         &lt;enumeration value="COM"/>
     *                         &lt;enumeration value="LPT"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="critical" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "origin",
        "price",
        "type",
        "critical"
    })
    public static class ComputerDevice {

        @XmlElement(required = true)
        protected String name;
        @XmlElement(required = true)
        protected String origin;
        protected int price;
        @XmlElement(required = true)
        protected Devices.ComputerDevice.Type type;
        @XmlElement(required = true)
        protected String critical;
        @XmlAttribute(name = "id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        @XmlSchemaType(name = "ID")
        protected String id;

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the origin property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOrigin() {
            return origin;
        }

        /**
         * Sets the value of the origin property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOrigin(String value) {
            this.origin = value;
        }

        /**
         * Gets the value of the price property.
         * 
         */
        public int getPrice() {
            return price;
        }

        /**
         * Sets the value of the price property.
         * 
         */
        public void setPrice(int value) {
            this.price = value;
        }

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link Devices.ComputerDevice.Type }
         *     
         */
        public Devices.ComputerDevice.Type getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link Devices.ComputerDevice.Type }
         *     
         */
        public void setType(Devices.ComputerDevice.Type value) {
            this.type = value;
        }

        /**
         * Gets the value of the critical property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCritical() {
            return critical;
        }

        /**
         * Sets the value of the critical property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCritical(String value) {
            this.critical = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="peripheral">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;pattern value="Yes|No|yes|no"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="energy_consumption" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="cooler" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="group" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="port">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="USB"/>
         *               &lt;enumeration value="HDMI"/>
         *               &lt;enumeration value="COM"/>
         *               &lt;enumeration value="LPT"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "peripheral",
            "energyConsumption",
            "cooler",
            "group",
            "port"
        })
        public static class Type {

            @XmlElement(required = true)
            protected String peripheral;
            @XmlElement(name = "energy_consumption")
            protected int energyConsumption;
            @XmlElement(required = true)
            protected String cooler;
            @XmlElement(required = true)
            protected String group;
            @XmlElement(required = true)
            protected String port;

            /**
             * Gets the value of the peripheral property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPeripheral() {
                return peripheral;
            }

            /**
             * Sets the value of the peripheral property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPeripheral(String value) {
                this.peripheral = value;
            }

            /**
             * Gets the value of the energyConsumption property.
             * 
             */
            public int getEnergyConsumption() {
                return energyConsumption;
            }

            /**
             * Sets the value of the energyConsumption property.
             * 
             */
            public void setEnergyConsumption(int value) {
                this.energyConsumption = value;
            }

            /**
             * Gets the value of the cooler property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCooler() {
                return cooler;
            }

            /**
             * Sets the value of the cooler property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCooler(String value) {
                this.cooler = value;
            }

            /**
             * Gets the value of the group property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGroup() {
                return group;
            }

            /**
             * Sets the value of the group property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGroup(String value) {
                this.group = value;
            }

            /**
             * Gets the value of the port property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPort() {
                return port;
            }

            /**
             * Sets the value of the port property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPort(String value) {
                this.port = value;
            }

        }

    }

}
