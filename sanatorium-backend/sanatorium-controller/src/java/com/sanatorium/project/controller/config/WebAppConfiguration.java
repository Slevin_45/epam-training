package com.sanatorium.project.controller.config;

import com.sanatorium.project.business.config.ServiceConfiguration;
import com.sanatorium.project.controller.security.AuthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@Import({ServiceConfiguration.class})
@EnableWebMvc
@ComponentScan(basePackages = {"com.sanatorium.project.controller"})
@EnableTransactionManagement
public class WebAppConfiguration extends WebMvcConfigurerAdapter {
    private static final String PROPERTY_NAME_RESOURCE_HANDLER = "/WEB-INF/pages/**";
    private static final String PROPERTY_NAME_RESOURCE_LOCATION = "/pages/";
    private static final String PROPERTY_NAME_PREFIX = "/WEB-INF/pages/";
    private static final String PROPERTY_NAME_SUFFIX = ".jsp";
    private static final String PROPERTY_NAME_WELCOME_FILE = "index";

    @Autowired
    private AuthInterceptor authInterceptor;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/").setCachePeriod(0);
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {

        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/**");
    }

}