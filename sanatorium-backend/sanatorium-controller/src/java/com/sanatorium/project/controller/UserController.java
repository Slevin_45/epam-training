package com.sanatorium.project.controller;

import com.sanatorium.project.business.AttendanceService;
import com.sanatorium.project.business.UserService;
import com.sanatorium.project.model.Attendace;
import com.sanatorium.project.model.User;
import dto.UserDto;
import dto.response.AttendancesResponseBean;
import dto.response.UserProfileResponseBean;
import dto.response.UsersResponseBean;
import org.omg.CORBA.Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * Created by Loki on 30.04.2016.
 */
@RestController
@RequestMapping(value = "/user")
public class UserController {
    private static final String NUMBER = "number";
    private static final String COUNT = "count";
    private static final String USER_ID = "userId";
    private static final String MONEY = "money";

    @Autowired
    UserService userService;
    @Autowired
    AttendanceService attendanceService;

    @RequestMapping(method = RequestMethod.GET, value = "/getAll")
    public ResponseEntity<UsersResponseBean> getUsers(@RequestParam(NUMBER) Integer number,
                                                     @RequestParam(required = false, value = COUNT) Integer count) {
        return new ResponseEntity<>(userService.getUsers(number, count), HttpStatus.OK);
    }


    @RequestMapping(value = "/attendance", method = RequestMethod.POST)
    public ResponseEntity<Object> addAttendace() {
        Integer attendanceId = 2;
        attendanceService.addAttendanceToUser(attendanceId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/attendances", method = RequestMethod.GET)
    public ResponseEntity<AttendancesResponseBean> getUsersAttendances(@RequestParam(NUMBER) Integer number,
                                                                       @RequestParam(required = false, value = COUNT) Integer count) {
        return new ResponseEntity<>(attendanceService.getUsersAttendances(number, count), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UserProfileResponseBean> getUsersProfile() {
        return new ResponseEntity<>(userService.getUsersProfile(), HttpStatus.OK);
    }




}
