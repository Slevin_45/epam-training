package com.sanatorium.project.controller;

import com.sanatorium.project.business.AttendanceService;
import com.sanatorium.project.business.UserService;
import dto.request.CreateAttendanceRequestBean;
import dto.request.LoginRequestBean;
import dto.response.AttendanceResponseBean;
import dto.response.AttendancesResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Loki on 30.04.2016.
 */
@RestController
@RequestMapping(value = "/attendance")
public class AttendanceController {
    private static final String NUMBER = "number";
    private static final String COUNT = "count";

    @Autowired
    AttendanceService attendanceService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AttendanceResponseBean> create(@RequestBody CreateAttendanceRequestBean bean) {
        return new ResponseEntity<>(attendanceService.createAttendance(bean), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<AttendancesResponseBean> getAttendnaces(@RequestParam(NUMBER) Integer number,
                                                                  @RequestParam(required = false, value = COUNT) Integer count) {
        return new ResponseEntity<>(attendanceService.getAttendances(number, count), HttpStatus.OK);
    }




}
