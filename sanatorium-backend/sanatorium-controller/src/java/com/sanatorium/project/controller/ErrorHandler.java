package com.sanatorium.project.controller;


import dto.ErrorCode;
import dto.response.ErrorResponseBean;
import exception.ApplicationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;



@ControllerAdvice
public class ErrorHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponseBean handleApplicationException(Exception e) {
        return handleException(e);
    }

    private ErrorResponseBean handleException(Exception e) {
        ErrorCode errorCode = getErrorCodeForException(e);
        String message = errorCode.getMessage();
        if (e.getLocalizedMessage() != null) {
            message = e.getLocalizedMessage();
        }
        return new ErrorResponseBean(errorCode.getErrorCode(), message);
    }


    private ErrorCode getErrorCodeForException(Exception e) {
        if (e instanceof ApplicationException) {
            return ((ApplicationException) e).getErrorCode();
        } else {
            return ErrorCode.INTERNAL_SERVER_ERROR;
        }
    }
}
