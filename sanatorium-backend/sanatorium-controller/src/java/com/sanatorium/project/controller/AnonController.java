package com.sanatorium.project.controller;

import com.sanatorium.project.business.UserService;
import dto.request.LoginRequestBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Loki on 30.04.2016.
 */
@RestController
@RequestMapping(value = "/anon")
public class AnonController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST) // works
    public ResponseEntity<Object> loginUser(@RequestBody LoginRequestBean bean) {
        return new ResponseEntity<>(userService.loginUser(bean), HttpStatus.CREATED);
    }
}
