package com.sanatorium.project.controller;

import com.sanatorium.project.business.AttendanceService;
import com.sanatorium.project.business.UserService;
import com.sanatorium.project.model.User;
import dto.request.RegistrationRequestBean;
import dto.response.AttendancesResponseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Loki on 30.04.2016.
 */
@RestController
@RequestMapping(value = "/admin")
public class AdminController {
    private static final String NUMBER = "number";
    private static final String COUNT = "count";

    @Autowired
    UserService userService;
    @Autowired
    AttendanceService attendanceService;


    @RequestMapping(value = "/register", method = RequestMethod.POST) // works
    public ResponseEntity<Object> registerUser(@RequestBody RegistrationRequestBean bean) {
        return new ResponseEntity<>(userService.registerUser(bean), HttpStatus.CREATED);
    }
    @RequestMapping(value = "/attendances/{userId}", method = RequestMethod.GET)
    public ResponseEntity<AttendancesResponseBean> getUsersAttendances(@RequestParam(NUMBER) Integer number,
                                                                       @RequestParam(required = false, value = COUNT) Integer count,
                                                                       @PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(attendanceService.getUsersAttendances(number, count, userId), HttpStatus.OK);
    }
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateUser(@PathVariable("id") int id, @RequestParam(value = "money") int money) {
        userService.updateUser(id, money);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
