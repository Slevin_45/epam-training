package com.sanatorium.project.business;

import com.sanatorium.project.model.User;
import dto.request.LoginRequestBean;
import dto.request.RegistrationRequestBean;
import dto.response.TokenResponseBean;
import dto.response.UserProfileResponseBean;
import dto.response.UsersResponseBean;

/**
 * Created by Loki on 30.04.2016.
 */
public interface UserService {
    User getUserByToken(String token);
    TokenResponseBean registerUser(RegistrationRequestBean bean);
    TokenResponseBean loginUser(LoginRequestBean bean);

    UsersResponseBean getUsers(Integer number, Integer count);

    void updateUser(Integer userId, Integer money);

    User findUser(int id);

    UserProfileResponseBean getUsersProfile();
}
