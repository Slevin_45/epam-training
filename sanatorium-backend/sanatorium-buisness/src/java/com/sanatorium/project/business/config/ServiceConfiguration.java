package com.sanatorium.project.business.config;

import com.sanatorium.project.dao.configuration.RepositoryConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by eldar.s on 21.04.2016.
 */
@Configuration
@ComponentScan(basePackages = {"com.sanatorium.project.business"})
@Import({RepositoryConfig.class })
public class ServiceConfiguration {
}
