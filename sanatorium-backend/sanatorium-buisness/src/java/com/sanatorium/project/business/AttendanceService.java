package com.sanatorium.project.business;

import dto.request.CreateAttendanceRequestBean;
import dto.response.AttendanceResponseBean;
import dto.response.AttendancesResponseBean;

/**
 * Created by Loki on 30.04.2016.
 */
public interface AttendanceService {
    AttendanceResponseBean createAttendance(CreateAttendanceRequestBean bean);

    AttendancesResponseBean getAttendances(Integer number, Integer count);

    void addAttendanceToUser(Integer attendanceId);

    AttendancesResponseBean getUsersAttendances(Integer number, Integer count, Integer userId);

    AttendancesResponseBean getUsersAttendances(Integer number, Integer count);

}
