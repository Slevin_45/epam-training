package com.sanatorium.project.business.impl;

import com.sanatorium.project.business.AttendanceService;
import com.sanatorium.project.business.security.SecurityContextHolder;
import com.sanatorium.project.business.utils.ConvertUtil;
import com.sanatorium.project.business.utils.ConvertedList;
import com.sanatorium.project.dao.impl.AttendanceDaoImpl;
import com.sanatorium.project.dao.impl.UserDaoImpl;
import com.sanatorium.project.model.Attendace;
import com.sanatorium.project.model.User;
import com.sanatorium.project.model.UserRole;
import dto.request.CreateAttendanceRequestBean;
import dto.response.AttendanceResponseBean;
import dto.response.AttendancesResponseBean;
import exception.AppAlreadyExistsException;
import exception.NotEnoughMoneyException;
import exception.PermissionDeniedException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by Loki on 30.04.2016.
 */
@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService {
    @Autowired
    private AttendanceDaoImpl attendanceDao;
    @Autowired
    private UserDaoImpl userDao;

    @Override
    public AttendanceResponseBean createAttendance(CreateAttendanceRequestBean bean) {
        if (!(SecurityContextHolder.getInstance().getUserRole().equals(UserRole.ROLE_ADMIN))) {
            throw new PermissionDeniedException();
        }
        Attendace attendace = new Attendace();
        attendace.setName(bean.getName());
        attendace.setCost(bean.getCost());
        attendace.setQuantity(bean.getQuantity());
        attendanceDao.persist(attendace);
        AttendanceResponseBean responseBean = new AttendanceResponseBean();
        responseBean.setAttendance(ConvertUtil.convert(attendace));
        return responseBean;
    }

    @Override
    public AttendancesResponseBean getAttendances(Integer number, Integer count) {
        AttendancesResponseBean bean = new AttendancesResponseBean();
        List<Attendace> attendaceList = attendanceDao.getAttendances(number, count);
        bean.setApplications(new ConvertedList<>(attendaceList));
        return bean;
    }

    @Override
    public void addAttendanceToUser(Integer attendanceId) {
        Attendace attendace = attendanceDao.findById(attendanceId);
        User user = userDao.findById(SecurityContextHolder.getInstance().getUserId());
        if(user.getMoney() < attendace.getCost())
            throw new NotEnoughMoneyException();
        Set<Attendace> attendaces = new HashSet<Attendace>();
        attendaces.add(attendace);
        user.setAttendaces(attendaces);
        userDao.persist(user);

    }

    @Override
    public AttendancesResponseBean getUsersAttendances(Integer number, Integer count, Integer userId) {
        AttendancesResponseBean bean = new AttendancesResponseBean();
        Collection<Object> attendaceList= attendanceDao.getUserAttendances(number, count, userId);
        bean.setApplications(new ConvertedList<>(attendaceList));
        return bean;
    }

    @Override
    public AttendancesResponseBean getUsersAttendances(Integer number, Integer count) {
        AttendancesResponseBean bean = new AttendancesResponseBean();
        Collection<Object> attendaceList= attendanceDao.getUserAttendances(number, count, SecurityContextHolder.getInstance().getUserId());
        bean.setApplications(new ConvertedList<>(attendaceList));
        return bean;
    }
}
