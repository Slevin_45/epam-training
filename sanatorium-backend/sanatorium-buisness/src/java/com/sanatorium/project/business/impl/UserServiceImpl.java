package com.sanatorium.project.business.impl;

import com.sanatorium.project.business.UserService;
import com.sanatorium.project.business.security.SecurityContextHolder;
import com.sanatorium.project.business.utils.ConvertedList;
import com.sanatorium.project.dao.impl.TokenDaoImpl;
import com.sanatorium.project.dao.impl.UserDaoImpl;
import com.sanatorium.project.model.Token;
import com.sanatorium.project.model.User;
import com.sanatorium.project.model.UserRole;
import dto.request.LoginRequestBean;
import dto.request.RegistrationRequestBean;
import dto.response.TokenResponseBean;
import dto.response.UserProfileResponseBean;
import dto.response.UsersResponseBean;
import exception.PermissionDeniedException;
import exception.UserAlreadyExistsException;
import exception.UserNotExistsException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    TokenDaoImpl tokenDao;
    @Autowired
    UserDaoImpl userDao;

    @Override
    public User getUserByToken(String token) {
        User user = tokenDao.getUserByToken(token);
        if (user == null) {
            return null;
        }
        return user;
    }

    @Override
    public TokenResponseBean registerUser(RegistrationRequestBean bean) {
        if(!(SecurityContextHolder.getInstance().getUserRole().equals(UserRole.ROLE_ADMIN)))
            throw new PermissionDeniedException();
        if (userDao.getUserByLogin(bean.getLogin()) != null) {
            throw new UserAlreadyExistsException();
        }
        User user = new User();
        user.setName(bean.getName());
        user.setLogin(bean.getLogin());
        user.setPassword(bean.getPassword());
        user.setMoney(bean.getMoney());
        user.setRole(UserRole.ROLE_CLIENT);
        userDao.persist(user);
        return new TokenResponseBean(createToken(user), user.getRole().name());
    }

    @Override
    public TokenResponseBean loginUser(LoginRequestBean bean) {
        User user = userDao.getUserByLoginAndPassword(bean.getLogin(), bean.getPassword());
        if (user == null) {
            throw new UserNotExistsException();
        }
        String r = String.valueOf(user.getRole());
        String role = user.getRole().name();
        return new TokenResponseBean(createToken(user), user.getRole().name());
    }

    @Override
    public UsersResponseBean getUsers(Integer number, Integer count) {
        if(!(SecurityContextHolder.getInstance().getUserRole().equals(UserRole.ROLE_ADMIN)))
            throw new PermissionDeniedException();
        UsersResponseBean bean = new UsersResponseBean();
        List<User> usersList = userDao.getUsers(number, count);
        bean.setUsers(new ConvertedList<>(usersList));
        return bean;
    }

    @Override
    public void updateUser(Integer userId, Integer money) {
        userDao.update(userId, money);
    }

    @Override
    public User findUser(int id) {
        return userDao.findById(id);
    }

    @Override
    public UserProfileResponseBean getUsersProfile() {
        User user = userDao.findById(SecurityContextHolder.getInstance().getUserId());
        UserProfileResponseBean bean = new UserProfileResponseBean();
        bean.setMoney(user.getMoney());
        bean.setSex(user.getSex());
        bean.setSurname(user.getSurname());
        bean.setPassword(user.getPassword());
        bean.setLogin(user.getLogin());
        bean.setName(user.getName());
        return bean;
    }

    private String createToken(User user) {
        String tokenData = DigestUtils.md5Hex((user.getLogin() + System.currentTimeMillis()).getBytes());
        Token token = new Token();
        token.setToken(tokenData);
        token.setUser(user);
        tokenDao.persist(token);
        return tokenData;

    }
}
