package com.sanatorium.project.business.utils;



import com.sanatorium.project.model.Attendace;
import com.sanatorium.project.model.User;
import dto.AttendanceDto;
import dto.BaseDto;
import dto.UserDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by eldar.s on 25.04.2016.
 */
public class ConvertUtil {

    public static <T extends BaseDto, R extends Object> List<T> convert(Collection<R> list) {
        if (list == null) {
            return null;
        }
        List<T> result = new ArrayList<>();
        for (R entity : list) {
            result.add((T) convert(entity));
        }
        return result;
    }

    private static <T , R extends Object> T convert(R identifier) {
        if (identifier == null)
            return null;
        if (identifier instanceof Attendace) {
            return (T) convert((Attendace) identifier);
        }
        else if(identifier instanceof User) {
            return (T) convert((User) identifier);
        }
        else {
            throw new UnsupportedOperationException(
                    "Method is not found: convert(" + identifier.getClass().getName() + ")");
        }
    }

    public static AttendanceDto convert(Attendace attendace) {
        AttendanceDto attendanceDto = new AttendanceDto();
        attendanceDto.setName(attendace.getName());
        attendanceDto.setCost(attendace.getCost());
        attendanceDto.setQuantity(attendace.getQuantity());
        return attendanceDto;
    }

    public static UserDto convert(User user){
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setMoney(user.getMoney());
        userDto.setLogin(user.getLogin());
        userDto.setPassword(user.getPassword());
        userDto.setSurname(user.getSurname());
        userDto.setSex(user.getSex());
        return userDto;
    }


}
