package dto;

import java.util.Date;

/**
 * Created by eldar.s on 25.04.2016.
 */
public class AttendanceDto extends BaseDto {
    private String name;
    private int quantity;
    private int cost;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
