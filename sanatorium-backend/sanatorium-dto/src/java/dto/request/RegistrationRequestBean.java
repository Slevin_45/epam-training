package dto.request;

/**
 * Created by eldar.s on 22.04.2016.
 */
public class RegistrationRequestBean {

    private int money;

    private String login;

    private String password;

    private String name;

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}
