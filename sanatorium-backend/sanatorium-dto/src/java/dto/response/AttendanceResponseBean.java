package dto.response;

import dto.AttendanceDto;

/**
 * Created by Loki on 30.04.2016.
 */
public class AttendanceResponseBean {
    private AttendanceDto attendance;

    public AttendanceDto getAttendance() {
        return attendance;
    }

    public void setAttendance(AttendanceDto attendance) {
        this.attendance = attendance;
    }
}
