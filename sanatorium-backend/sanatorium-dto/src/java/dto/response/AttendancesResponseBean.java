package dto.response;

import dto.AttendanceDto;

import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
public class AttendancesResponseBean {
    private List<AttendanceDto> applications;

    public List<AttendanceDto> getApplications() {
        return applications;
    }

    public void setApplications(List<AttendanceDto> applications) {
        this.applications = applications;
    }
}
