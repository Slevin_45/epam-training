package dto.response;

import dto.AttendanceDto;
import dto.UserDto;

import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
public class UsersResponseBean {
    private List<UserDto> users;

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }
}
