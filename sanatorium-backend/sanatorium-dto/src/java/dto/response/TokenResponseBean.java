package dto.response;

/**
 * Created by eldar.s on 26.04.2016.
 */
public class TokenResponseBean extends BaseResponseBean {

    private String token;
    private String role;

    public TokenResponseBean(String token, String role) {
        this.token = token;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public TokenResponseBean(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
