package exception;

import dto.ErrorCode;

/**
 * Created by Loki on 30.04.2016.
 */
public class PermissionDeniedException extends ApplicationException {
    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.PERSMISSION_DENIED_ERROR;
    }
}
