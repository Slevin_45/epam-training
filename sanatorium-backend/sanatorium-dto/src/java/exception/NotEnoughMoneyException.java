package exception;

import dto.ErrorCode;

/**
 * Created by Loki on 30.04.2016.
 */
public class NotEnoughMoneyException extends ApplicationException {
    @Override
    public ErrorCode getErrorCode() {
        return ErrorCode.NOT_ENOUGH_MONEY_ERROR;
    }
}
