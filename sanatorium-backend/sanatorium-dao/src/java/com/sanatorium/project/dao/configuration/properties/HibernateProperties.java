package com.sanatorium.project.dao.configuration.properties;

import java.util.Map;

public interface HibernateProperties {

    public Map<String, String> propertiesMap();
    
}
