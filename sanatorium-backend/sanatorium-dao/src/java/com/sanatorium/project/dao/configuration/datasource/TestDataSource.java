package com.sanatorium.project.dao.configuration.datasource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.sql.DataSource;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.04.16
 * Time: 0:11
 * To change this template use File | Settings | File Templates.
 */
@PropertySource("classpath:jdbc.properties")
@Configuration
public class TestDataSource {

    @Bean
    public DataSource datasource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/Sanatorium");
        dataSource.setUsername("postgres");
        dataSource.setPassword("fotboll1995");

        return dataSource;
    }

}
