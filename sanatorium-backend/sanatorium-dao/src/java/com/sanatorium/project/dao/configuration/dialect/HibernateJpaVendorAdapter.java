package com.sanatorium.project.dao.configuration.dialect;



public class HibernateJpaVendorAdapter extends org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter {

    private HibernateJpaDialect dialect;

    @Override
    public HibernateJpaDialect getJpaDialect() {
        dialect = new HibernateJpaDialect();
        return dialect;
    }
}
