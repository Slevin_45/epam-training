package com.sanatorium.project.dao.impl;


import com.sanatorium.project.dao.AttendanceDao;
import com.sanatorium.project.dao.BaseDao;
import com.sanatorium.project.model.Attendace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.support.SecurityContextProvider;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
@Repository
public class AttendanceDaoImpl extends BaseDao<Attendace> implements AttendanceDao {
    private static final int PAGE_SIZE_10 = 10;
    public AttendanceDaoImpl() {
        setClazz(Attendace.class);
    }
    @Override
    public List<Attendace> getAttendances(Integer number, Integer count) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Attendace> criteria = builder.createQuery(Attendace.class);
        Root<Attendace> from = criteria.from(Attendace.class);
        TypedQuery<Attendace> query = entityManager.createQuery(criteria);
        query.setFirstResult(number);
        query.setMaxResults(count == null ? PAGE_SIZE_10 : count);
        if(query.getResultList().isEmpty())
            return null;
        return query.getResultList();
    }

    @Override
    public Collection<Object> getUserAttendances(Integer number, Integer count, Integer userId) {
        Session session = (Session) entityManager.getDelegate();
        Collection<Object> result = new LinkedHashSet<Object>(session.createCriteria(Attendace.class)
                .createAlias("users", "usersAlias")
                .add(Restrictions.eq("usersAlias.id", userId))
                .setFirstResult(number)
                .setMaxResults(count == null ? PAGE_SIZE_10 : count).list());
        return result;
    }

}
