package com.sanatorium.project.dao.impl;


import com.sanatorium.project.dao.BaseDao;
import com.sanatorium.project.dao.UserDao;
import com.sanatorium.project.model.Attendace;
import com.sanatorium.project.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
@Repository
public class UserDaoImpl extends BaseDao<User> implements UserDao {
    private static final int PAGE_SIZE_10 = 10;

    public UserDaoImpl() {
        setClazz(User.class);
    }

    public User getUserByLoginAndPassword(String login, String password) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> userRoot = criteria.from(User.class);
        criteria.select(userRoot);
        criteria.where(
                builder.equal(userRoot.get("login"), login),
                builder.equal(userRoot.get("password"), password)
        );
        TypedQuery<User> query = entityManager.createQuery(criteria);
        List<User> results = query.getResultList();
        if (results.isEmpty())
            return null;
        return results.get(0);
    }


    public User getUserByLogin(String login) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> userRoot = criteria.from(User.class);
        criteria.select(userRoot).where(builder.equal(userRoot.get("login"), login));
        TypedQuery<User> query = entityManager.createQuery(criteria);
        List<User> results = query.getResultList();
        if (results.isEmpty())
            return null;
        return results.get(0);
    }

    @Override
    public List<User> getUsers(Integer number, Integer count) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> from = criteria.from(User.class);
        TypedQuery<User> query = entityManager.createQuery(criteria);
        query.setFirstResult(number);
        query.setMaxResults(count == null ? PAGE_SIZE_10 : count);
        if (query.getResultList().isEmpty())
            return null;
        return query.getResultList();
    }

    @Override
    public void update(int id, int money) {
        Query query = entityManager.createQuery("update User t set t.money=?1 where t.id=?2");
        query.setParameter(1, money);
        query.setParameter(2, id);
        query.executeUpdate();
    }

}
