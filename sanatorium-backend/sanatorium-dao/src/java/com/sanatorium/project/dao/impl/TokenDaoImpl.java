package com.sanatorium.project.dao.impl;

import com.sanatorium.project.dao.BaseDao;
import com.sanatorium.project.dao.TokenDao;
import com.sanatorium.project.model.Token;
import com.sanatorium.project.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
@Repository
public class TokenDaoImpl extends BaseDao<Token> implements TokenDao {

    public TokenDaoImpl() {
        setClazz(Token.class);
    }

    @Override
    public User getUserByToken(String token) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Token> criteria = builder.createQuery(Token.class);
        Root<Token> from = criteria.from(Token.class);
        criteria.select(from).where(builder.equal(from.get("token"), token));
        TypedQuery<Token> query = entityManager.createQuery(criteria);
        List<Token> results = query.getResultList();
        if (results.isEmpty())
            return null;
        return results.get(0).getUser();
    }
}
