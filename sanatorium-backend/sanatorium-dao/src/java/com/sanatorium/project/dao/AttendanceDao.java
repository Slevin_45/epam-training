package com.sanatorium.project.dao;

import com.sanatorium.project.model.Attendace;

import java.util.Collection;
import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
public interface AttendanceDao {
    List<Attendace> getAttendances(Integer number, Integer count);
    Collection<Object> getUserAttendances(Integer number, Integer count, Integer userId);
}
