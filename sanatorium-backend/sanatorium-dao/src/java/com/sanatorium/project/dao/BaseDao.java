package com.sanatorium.project.dao;

import org.omg.CORBA.Object;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by eldar.s on 27.04.2016.
 */
@Repository
public abstract class BaseDao<T> {
    private Class< T > clazz;

    @PersistenceContext
    protected EntityManager entityManager;

    public final void setClazz( Class< T > clazzToSet ){
        this.clazz = clazzToSet;
    }


    public void persist(T entity) {
        entityManager.persist(entity);
    }

    public void remove(T entity) {
        entityManager.remove(entity);
    }

    public T findById(Integer id) {
        return (T) entityManager.find(clazz, id);
    }


}
