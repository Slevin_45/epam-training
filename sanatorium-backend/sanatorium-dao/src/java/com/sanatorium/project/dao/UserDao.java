package com.sanatorium.project.dao;

import com.sanatorium.project.model.User;

import java.util.List;

/**
 * Created by Loki on 30.04.2016.
 */
public interface UserDao {
    User getUserByLoginAndPassword(String login, String password);
    User getUserByLogin(String login);

    List<User> getUsers(Integer number, Integer count);

    void update(int id, int money);

}
