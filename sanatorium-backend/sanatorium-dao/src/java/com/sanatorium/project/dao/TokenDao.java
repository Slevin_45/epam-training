package com.sanatorium.project.dao;

import com.sanatorium.project.model.User;

/**
 * Created by Loki on 30.04.2016.
 */
public interface TokenDao {
    User getUserByToken(String token);
}
