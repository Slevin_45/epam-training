package com.sanatorium.project.model;

import javax.persistence.*;

/**
 * Created by Loki on 30.04.2016.
 */
@Entity
@Table(name = "TOKEN")
public class Token extends Identifier{

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="USERID")
    private User user;

    @Column(name = "TOKEN")
    private String token;


    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="USERID")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
