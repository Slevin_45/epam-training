package com.sanatorium.project.model;

public enum UserRole {

    ROLE_ADMIN, ROLE_CLIENT

}
