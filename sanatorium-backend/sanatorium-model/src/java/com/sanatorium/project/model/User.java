package com.sanatorium.project.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Loki on 30.04.2016.
 */
@Entity
@Table(name = "USERS")
public class User extends Identifier{

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
    private List<Token> tokens;

    @Column(name = "NAME")
    private String name;

    @Column(name = "Surname")
    private String surname;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "money")
    private int money;

    @Column(name = "sex")
    private String sex;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_attendance", joinColumns = {
            @JoinColumn(name = "USERID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "ATTENDANCEID",
                    nullable = false, updatable = false) })
    private Set<Attendace> attendaces = new HashSet<Attendace>(0);

    public Set<Attendace> getAttendances() {
        return this.attendaces;
    }

    public void setAttendaces(Set<Attendace> attendaces) {
        this.attendaces = attendaces;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
