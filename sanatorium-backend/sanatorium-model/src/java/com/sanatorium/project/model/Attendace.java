package com.sanatorium.project.model;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Loki on 30.04.2016.
 */
@Entity
@Table(name = "ATTENDANCE")
public class Attendace extends Identifier{
    @Column(name = "NAME")
    private String name;

    @Column(name = "COST")
    private Integer cost;

    @Column(name = "QUANTITY")
    private Integer quantity;


    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "attendaces")
    private Set<User> users = new HashSet<User>(0);


    public Set<User> getUsers() {
        return this.users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
