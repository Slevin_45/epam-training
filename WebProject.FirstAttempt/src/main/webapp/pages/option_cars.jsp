<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<html>
<head>
    <meta charset="UTF-8">
    <title>AdminCars</title>
</head>
<body>
<style>
    html, body {
        height: 100%;
    }
    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff8ef', endColorstr='#dddddd',GradientType=0 ); /* IE6-9 */
    }

    .cars {
        display: block;
    }
    .cars__car {
        display: inline-block;
        padding: 5px 10px;
    }
    .car__price {
        text-transform: uppercase;
        color: #d23336;
        font-weight: bold;
        font-size: 30px;

        padding: 10px 0;
    }
    .car__actions {
        font-size: 0;
    }
    .actions__action {
        font-size: 15px;
        line-height: 17px;
        color: #2a6496;
        text-decoration: blink;
        border: 1px solid;
        padding: 3px 6px;
        transition: all .15s ease;
    }
    .actions__action:hover {
        background: #2a6496;
        border-color: #2a6496;
        color: #fff;
    }
    .actions__action:first-child {
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .actions__action:last-child {
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
    }
    .actions__action:nth-child(n+2) {
        border-left: none;
    }

    .add-car {
        position: fixed;
    }
    .add-car__link {
        font-size: 30px;
        line-height: 30px;
        display: block;
        width: 30px;
        height: 30px;
        text-decoration: blink;
        text-align: center;
        color: #fff;
        background: #d23336;
        border: 1px solid #d23336;
        border-radius: 15px;
        opacity: .7;
        transition: opacity .15s ease;
    }
    .add-car__link:hover {
        opacity: 1;
    }

</style>
<script type="text/javascript">
    function dangerousMessage(){
        alert("Be carefully! This car can be ordered by somebody.")
    }
</script>
<jsp:include page="/pages/option_header.jsp" />
<form>
    <div class="lang_ch" style="opacity: 1;">
        <a class="active" href="/changeLang?language=ru">RU</a>
        <a href="/changeLang?language=en">EN</a>
    </div>
</form>
<div class="add-car">
    <a href="/pages/addCar.jsp" class="add-car__link">+</a>
</div>
<center>
    <div class="cars">
        <c:forEach var="car" items="${cars}">
            <div class="cars__car">
                <div class="car__price">${car.carType.model}</div>
                <img src="/images/${car.image_src}" width="245" height="126"/>
                <div class="car__price">${car.basePricePerDay}$</div>
                <div class="car__actions">
                    <a href="/cars/more?carId=${car.id}" onclick="return dangerousMessage();" class="actions__action"><fmt:message key="car.button.change"/></a>
                    <a href="/admin/cars/delete?carId=${car.id}" onclick="return dangerousMessage();" class="actions__action"><fmt:message key="car.button.delete"/></a>
                    <a href="/admin/cars/carBookings?carId=${car.id}" class="actions__action"><fmt:message key="car.button.showBookings"/></a>
                </div>
            </div>
        </c:forEach>
    </div>
</center>

</body>
</html>

