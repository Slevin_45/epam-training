<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<html lang="en">
<head>
    <title>Elcars</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html{
        height: inherit;
    }
    body {
        height: 100%;
    }

    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff8ef', endColorstr='#dddddd', GradientType=0); /* IE6-9 */
    }

    .carousel-inner img {
        margin: 0 auto;
    }

    .container {
        font-size: 0;
        line-height: 0;
        max-width: 860px;
        margin-top: 13px;
    }

    .container__title {
        font-size: 21px;
        line-height: 25px;
        text-align: center;
    }

    .container__advent {
        display: block;
    }

    .advent__item {
        display: inline-block;
        width: 50%;
        text-align: left;
    }

    .advent__icon {
        width: 30px;
        height: 30px;
        display: inline-block;
    }

    .advent__defn {
        font-size: 16px;
        line-height: 18px;
        padding-bottom: 3px;
        padding-top: 3px;
    }

    .advent__desc {
        font-size: 14px;
        line-height: 16px;
    }

    .alert {
        font-size: 18px;
        line-height: 30px;
        width: 100%;
        height: 30px;
        position: absolute;
        top: 0;
        left: 0;
    }

    .advent__img {
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        vertical-align: middle;
    }
</style>
<jsp:include page="/pages/header.jsp"/>
<form>
    <div class="lang_ch">
        <a class="active" href="/changeLang?language=ru">RU</a>
        <a href="/changeLang?language=en">EN</a>
    </div>
</form>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="/images/santaFe_carousel.png" alt="Jetta">

            <div class="carousel-caption">
                <h3>Polo</h3>
            </div>
        </div>

        <div class="item">
            <img src="/images/gmc_carousel.png" alt="Pagero">

            <div class="carousel-caption">
                <h3>Polo</h3>
            </div>
        </div>

        <div class="item">
            <img src="/images/ford_carousel.png" alt="Puma">

            <div class="carousel-caption">
                <h3>Accent</h3>
            </div>
        </div>

        <div class="item">
            <img src="/images/jeep_carousel.png" alt="Jetta">

            <div class="carousel-caption">
                <h3>Tuscon</h3>
            </div>
        </div>

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="container">
    <div class="container__title"><fmt:message key="index.icons.advantages"/></div>
    <ul class="container__advent">
        <li class="advent__item">
            <img class=advent__img src="/images/quality.svg" width="64" height="64">
            <span class="advent__cont">
                <div class="advent__defn"><fmt:message key="index.icons.quality"/></div>
                <div class="advent__desc"><fmt:message key="index.icons.qualityDescript"/></div>
            </span>
        </li>
        <li class="advent__item">
            <img class=advent__img src="/images/speed.svg" width="64" height="64">
            <span class="advent__cont">
                <div class="advent__defn"><fmt:message key="index.icons.speed"/></div>
                <div class="advent__desc"><fmt:message key="index.icons.speedDescript"/></div>
            </span>
        </li>
        <li class="advent__item">
            <img class=advent__img src="/images/cost.svg" width="64" height="64">
            <span class="advent__cont">
                <div class="advent__defn"><fmt:message key="index.icons.cost"/></div>
                <div class="advent__desc"><fmt:message key="index.icons.costDescript"/></div>
            </span>
        </li>
        <li class="advent__item">
            <img class=advent__img src="/images/reliability.svg" width="64" height="64">
            <span class="advent__cont">
                <div class="advent__defn"><fmt:message key="index.icons.reliability"/></div>
                <div class="advent__desc"><fmt:message key="index.icons.reliabilityDescript"/></div>
            </span>
        </li>
    </ul>
    <p class="container__text container__text_left">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Aspernatur, at autem
        consequatur deserunt dolore eligendi est illo iste nam odio pariatur placeat, quisquam quod repellat,
        reprehenderit rerum saepe ullam voluptatibus!</p>

    <p class="container__text container__test_right">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Aspernatur, hic?</p>
</div>
</body>
</html>
