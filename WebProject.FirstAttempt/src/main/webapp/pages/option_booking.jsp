<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html, body {
        height: 100%;
    }

    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff8ef', endColorstr='#dddddd', GradientType=0); /* IE6-9 */
    }
</style>
<jsp:include page="/pages/option_header.jsp"/>
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th><fmt:message key="booking.info.bookingNumber"/></th>
            <th><fmt:message key="booking.info.bookingDate"/></th>
            <th><fmt:message key="booking.info.returnDate"/></th>
            <th><fmt:message key="booking.info.status"/></th>
            <th><fmt:message key="car.info.carId"/></th>
            <th><fmt:message key="customer.info.user"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="booking" items="${bookings}">
            <tr>
                <form>
                    <td>${booking.bookingNumber}
                        <input type="hidden" name="bookingNumber" id="bookingNumber" value="${booking.bookingNumber}">
                    </td>
                    <td><input type="date" name="bookingDate" id="bookingDate" disabled=true
                               value= ${booking.bookingDate}></td>
                    <td><input type="date" name="returnDate" id="returnDate" disabled=true value= ${booking.returnDate}>
                    </td>
                    <td>${booking.status}</td>
                    <td>${booking.carId}</td>
                    <td><a href="/getDescription?login=${booking.customerLogin}">${booking.customerLogin}</a></td>
                    <input type="hidden" name="carId" type="text" value="${booking.carId}">
                    <c:if test="${booking.status == 'Checking'}">
                        <td>
                            <button type="submit"
                                    formaction="/secured/bookingDelete?bookingNumber=${booking.bookingNumber}"
                                    class="btn btn-danger"><fmt:message key="booking.button.refuse"/>
                            </button>
                        </td>
                        <td>
                            <button type="submit" formaction="/admin/bookingApprove?${booking.bookingNumber}"
                                    class="btn btn-success"><fmt:message key="booking.button.approve"/>
                            </button>
                        </td>
                    </c:if>
                </form>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>

