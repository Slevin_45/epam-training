<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="utf-8">
    <style type="text/css">
        html, body {
            height: 100%;
        }
        body {
            font-family: sans-serif;

            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
            background: #fff8ef; /* Old browsers */
            background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff8ef', endColorstr='#dddddd',GradientType=0 ); /* IE6-9 */
        }

        .form_left {
            display: inline-block;
            width: calc(100% - 250px);
            vertical-align: middle
        }
        .form_right {
            display: inline-block;
            width: 240px;
            vertical-align: middle;
        }
        .car__img {
            width: 240px;
        }
    </style>
</head>
<body>
<jsp:include page="/pages/option_header.jsp" />
<div class="container">
<form action="/admin/cars/carUpdate" class="form-horizontal" role="form">
    <div class="form_left">
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.regNumber"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "registration_number" id="registration_number" type="text" value="${car.registrationNumber}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.dateOfManufacturing"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "date_of_manufacturing" id="date_of_manufacturing" type="text" value="${car.dateOfManufacturing}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.price"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "base_price_per_day" id="base_price_per_day" type="text" value="${car.basePricePerDay}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.model"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "model" id="model" type="text" value="${car.carType.model}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.producer"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "producer" id="Producer" type="text" value="${car.carType.producer}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label"><fmt:message key="car.characteristics.transmission"/></label>
            <div class="col-sm-10">
                <input class="form-control" name = "transmission" id="transmission" type="text" value="${car.carType.transmission}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><fmt:message key="car.button.submit"/></button>
            </div>
        </div>
    </div>
    <div class="form_right">
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <img class="car__img" src="/images/${car.image_src}"/>
            </div>
        </div>
    </div>
    </form>
  </div>
</body>
</html>