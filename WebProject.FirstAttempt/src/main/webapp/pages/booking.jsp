<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html, body {
        height: 100%;
    }
    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff8ef', endColorstr='#dddddd',GradientType=0 ); /* IE6-9 */
    }
 </style>
<jsp:include page="/pages/header.jsp" />
<div class="container">
    <table class="table table-hover">
        <thead>
        <tr>
            <th></th>
            <th><fmt:message key="booking.info.bookingNumber"/></th>
            <th><fmt:message key="booking.info.bookingDate"/></th>
            <th><fmt:message key="booking.info.returnDate"/></th>
            <th><fmt:message key="booking.info.status"/></th>
            <th><fmt:message key="car.info.carId"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="booking" items="${bookings}">
            <tr>
                <form name="editForm" id="editForm">
                    <td>
                        <button type="submit" formaction="/secured/bookingDelete" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </td>
                    <td>${booking.bookingNumber}
                        <input type="hidden" name="bookingNumber" id="bookingNumber" value="${booking.bookingNumber}">
                    </td>
                    <td><input type="date" name="bookingDate" id="bookingDate" disabled=true
                               value= ${booking.bookingDate}></td>
                    <td><input type="date" name="returnDate" id="returnDate" disabled=true value= ${booking.returnDate}>
                    </td>
                    <td>${booking.status}</td>
                    <td>${booking.carId}</td>
                    <input type="hidden" name="carId" type="text" value="${booking.carId}">
                    <td>
                        <c:if test="${booking.status == 'Checking'}">
                            <input type="button" value="<fmt:message key="booking.button.edit"/>" id="editButton"
                                   onclick="changeButton();">
                            <script>
                                var smartButton = document.getElementById("editButton");
                                var myForm = document.getElementById("editForm");
                                function changeButton() {
                                    if (smartButton.value == "<fmt:message key="booking.button.edit"/>") {
                                        document.getElementById("bookingDate").disabled = false;
                                        document.getElementById("returnDate").disabled = false;
                                        smartButton.value = "<fmt:message key="car.button.submit"/>";
                                    }
                                    else {
                                        myForm.action = '/secured/bookingUpdate?bookingNumber=${booking.bookingNumber}&carId=${booking.carId}';
                                        myForm.submit();
                                        document.getElementById("bookingDate").disabled = true;
                                        document.getElementById("returnDate").disabled = true;
                                        smartButton.value = "<fmt:message key="booking.button.edit"/>";
                                    }
                                }
                            </script>
                        </c:if>
                    </td>
                    </form>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
