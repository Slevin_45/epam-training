<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html{
        height: inherit;
    }
    body {
        height: 100%;
    }

    body {
    font-family: sans-serif;

    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
    background: #fff8ef; /* Old browsers */
    background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff8ef', endColorstr='#dddddd', GradientType=0); /* IE6-9 */
}
</style>
<jsp:include page="/pages/option_header.jsp"/>
<div class="container">
    <form action="/admin/returnCar" method="get">


        <input type="text" name="customer" id="registNumber" class="form-control input-sm"
               placeholder="<fmt:message key="registCar.label.regNumber"/>">


        <input type="text" name="bookingNumber" id="bookingNumber" class="form-control input-sm"
               placeholder="<fmt:message key="registCar.label.bookingNumber"/>">


        <input type="text" name="description" id="description" class="form-control input-sm"
               placeholder="<fmt:message key="registCar.label.descript"/>">


        <textarea class="form-control" rows="5" id="emailMsg" name="emailMsg" placeholder="<fmt:message key="registCar.label.Emailmsg"/>" ></textarea>

        <div class="checkbox">
            <label><input type="checkbox" name="broke" value="broke"><fmt:message key="registCar.label.broke"/></label>
        </div>


            <button type="submit" class="btn btn-default"><fmt:message key="registCar.label.submit"/></button>

    </form>
</div>
</div>
</div>
</body>
</html>
