<%--
  Created by IntelliJ IDEA.
  User: Loki
  Date: 07.02.16
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ElCars</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#"><fmt:message key="navbar.href.home"/></a></li>
                <li><a href="/cars"><fmt:message key="navbar.href.cars"/></a></li>
                <li><a href="/secured/bookings"><fmt:message key="navbar.href.bookings"/></a></li>
                <li><a href="/pages/registCar.jsp"><fmt:message key="navbar.href.registCar"/></a></li>
                <li><a href="tel:+375296361723" class="phone">+375 (29) 290-09-08</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/pages/login.jsp"><span class="glyphicon glyphicon-log-in"></span><fmt:message key="login.label.username"/></a></li>
                <li><a href="/logout"><span class="glyphicon glyphicon-log-out"></span><fmt:message key="navbar.href.logout"/></a></li>
            </ul>
        </div>
    </div>
</nav>

</body>
</html>