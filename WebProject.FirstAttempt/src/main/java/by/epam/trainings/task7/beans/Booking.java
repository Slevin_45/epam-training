package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 22:59
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;

@Entity
@Table(name = "BOOKING")
public final class Booking {
    private String bookingDate;
    private int bookingNumber;
    private String returnDate;
    private String status;
    private String customerLogin;
    private int carId;
    private int customerId;
    private String customerPass_date_of_issue;
    private String customerPass_number;
    private String customerPass_expiry;

    public Booking(int customerId, int carId, String bookingDate, String returnDate, String customerPass_date_of_issue,
                   String customerPass_number, String customerPass_expiry) {
        this.bookingDate = bookingDate;
        this.bookingNumber = bookingNumber;
        this.returnDate = returnDate;
        this.status = status;
        this.customerLogin = customerLogin;
        this.carId = carId;
        this.customerId = customerId;
    }

    public Booking() {

    }

    public String getCustomerPass_date_of_issue() {
        return customerPass_date_of_issue;
    }

    public void setCustomerPass_date_of_issue(String customerPass_date_of_issue) {
        this.customerPass_date_of_issue = customerPass_date_of_issue;
    }

    public String getCustomerPass_number() {
        return customerPass_number;
    }

    public void setCustomerPass_number(String customerPass_number) {
        this.customerPass_number = customerPass_number;
    }

    public String getCustomerPass_expiry() {
        return customerPass_expiry;
    }

    public void setCustomerPass_expiry(String customerPass_expiry) {
        this.customerPass_expiry = customerPass_expiry;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {

        return status;
    }

    @Column(name = "BOOKING_DATE", updatable = true, nullable = false)
    public String getBookingDate() {
        return bookingDate;
    }

    @Id
    @GeneratedValue
    @Column(name = "BOOKING_NUMBER", updatable = false, nullable = false)
    public Integer getBookingNumber() {
        return bookingNumber;
    }

    @Column(name = "RETURN_DATE", updatable = true, nullable = false)
    public String getReturnDate() {
        return returnDate;
    }

    @ManyToOne(optional = false, targetEntity = Customer.class)
    @JoinColumn(name = "CUSTOMER_ID", updatable = false, nullable = false, referencedColumnName = "ID")
    public String getCustomerLogin() {
        return customerLogin;
    }

    @ManyToOne(optional = false, targetEntity = Car.class)
    @JoinColumn(name = "CAR_ID", updatable = false, nullable = false, referencedColumnName = "ID")
    public Integer getCarId() {
        return carId;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public void setBookingNumber(Integer bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public void setCustomer(String customerLogin) {
        this.customerLogin = customerLogin;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

}

