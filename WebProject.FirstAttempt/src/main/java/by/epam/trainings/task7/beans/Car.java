package by.epam.trainings.task7.beans;

import javax.persistence.*;

@Entity
@Table(name = "CAR")
public final class Car {
    private String image_src;
    private double basePricePerDay;
    private String dateOfManufacturing;
    private Integer id;
    private String registrationNumber;
    private CarType carType;

    public Car(CarType carType, String registNumber, String dateOfManufacturing, String imageSrc, double price) {
        this.carType = carType;
        this.registrationNumber = registNumber;
        this.dateOfManufacturing = dateOfManufacturing;
        this.image_src = imageSrc;
        this.basePricePerDay = price;
    }

    public Car() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public Car(CarType carType, int carId, String registNumber, String dateOfManufacturing, double price) {
        this.carType = carType;
        this.id = carId;
        this.registrationNumber = registNumber;
        this.dateOfManufacturing = dateOfManufacturing;
        this.basePricePerDay = price;
    }

    @Column(name = "BASE_PRICE_PER_DAY", updatable = true, nullable = false)
    public double getBasePricePerDay() {
        return basePricePerDay;
    }

    @Column(name = "DATE_OF_MANUFACTURING", updatable = false, nullable = false)
    public String getDateOfManufacturing() {
        return dateOfManufacturing;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID", updatable = false, nullable = false)
    public Integer getId() {
        return id;
    }

    @Column(name = "REGISTRATION_NUMBER", updatable = false, nullable = false)
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    @ManyToOne(optional = false, targetEntity = CarType.class)
    @JoinColumn(name = "CAR_TYPE_ID", updatable = false, nullable = false, referencedColumnName = "ID")
    public CarType getCarType() {
        return carType;
    }

    public String getImage_src() {
        return image_src;
    }

    public void setImage_src(String image_src) {
        this.image_src = image_src;
    }

    public void setBasePricePerDay(Double basePricePerDay) {
        this.basePricePerDay = basePricePerDay;
    }

    public void setDateOfManufacturing(String dateOfManufacturing) {
        this.dateOfManufacturing = dateOfManufacturing;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }
}