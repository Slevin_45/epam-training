package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
public class ConfirmBookingCommand implements ICommand {
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";
    private static final String PARAM_NAME_BOOKING_NUMBER = "bookingNumber";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        int bookingNumber = Integer.parseInt(request.getParameter(PARAM_NAME_BOOKING_NUMBER));
        try {
            bookingDAO.confirmBooking(bookingNumber);
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
