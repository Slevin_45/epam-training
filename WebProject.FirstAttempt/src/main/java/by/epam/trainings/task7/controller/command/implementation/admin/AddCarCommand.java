package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.beans.CarType;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:52
 * To change this template use File | Settings | File Templates.
 */
public class AddCarCommand implements ICommand {
    private static final String MESSAGE = "message";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String PARAM_NAME_CAR_PRODUCER = "producer";
    private static final String SQL_CAR_ID = "id";
    private static final String PARAM_NAME_CAR_PRICE = "base_price_per_day";
    private static final String PARAM_NAME_CAR_TRANSMISSION = "transmission";
    private static final String PARAM_NAME_CAR_DATE_OF_MANUFACTURING = "date_Of_manufacturing";
    private static final String PARAM_NAME_CAR_REGIST_NUMBER = "registration_number";
    private static final String PARAM_NAME_CAR_MODEL = "model";
    private static final String PARAM_NAME_CAR_PICTURE = "pic";
    private static final String CARS = "cars";
    private static final String OPTION_CARS_PAGE = "path.page.option_cars";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        CarDAO carDAO = DAOFactory.getCarDAO();
        String model = request.getParameter(PARAM_NAME_CAR_MODEL);
        String producer = request.getParameter(PARAM_NAME_CAR_PRODUCER);
        String transmission = request.getParameter(PARAM_NAME_CAR_TRANSMISSION);
        String registNumber = request.getParameter(PARAM_NAME_CAR_REGIST_NUMBER);
        String dateOfManufacturing = request.getParameter(PARAM_NAME_CAR_DATE_OF_MANUFACTURING);
        String imageSrc = request.getParameter(PARAM_NAME_CAR_PICTURE);
        double price = Double.parseDouble(request.getParameter(PARAM_NAME_CAR_PRICE));
        CarType carType = new CarType(model, producer, transmission);
        Car car = new Car(carType, registNumber, dateOfManufacturing, imageSrc, price);
        try {
            carDAO.create(car);
            List<Car> carList = carDAO.findAll();
            request.setAttribute(CARS, carList);
            page = ConfigurationManager.getProperty(OPTION_CARS_PAGE);
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
