package by.epam.trainings.task7.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 03.01.16
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (((HttpServletRequest) servletRequest).getSession().getAttribute("user") != null) {
            filterChain.doFilter(servletRequest, servletResponse); // User is logged in, just continue request.
        } else {
            ((HttpServletResponse) servletResponse).sendRedirect("/pages/login.jsp"); // Not logged in, show login page. You can eventually show the error page instead.
        }
    }

    @Override
    public void destroy() {
    }
}
