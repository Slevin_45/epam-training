package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 22:59
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;

@Entity
@Table(name = "CAR_TYPE")
public final class CarType {
    private String transmission;
    private Integer id;
    private String model;
    private String producer;

    public CarType(String model, String producer, String transmission) {
        this.model = model;
        this.producer = producer;
        this.transmission = transmission;
    }

    public CarType() {

    }

    @Column(name = "AUTOMATIC", updatable = true, nullable = false)
    public String getTransmission() {
        return transmission;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID", updatable = false, nullable = false)
    public Integer getId() {
        return id;
    }

    @Column(name = "MODEL", updatable = true, nullable = false)
    public String getModel() {
        return model;
    }

    @Column(name = "PRODUCER", updatable = true, nullable = false)
    public String getProducer() {
        return producer;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }
}
