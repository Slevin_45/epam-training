package by.epam.trainings.task7.dao.impl;

import by.epam.trainings.task7.beans.Customer;
import by.epam.trainings.task7.connectionFactory.ConnectionFactory;
import by.epam.trainings.task7.connectionFactory.ConnectionFactoryException;
import by.epam.trainings.task7.dao.CustomerDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.passwordLogic.PasswordHasher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {
    private final static String SQL_PASSWORD_BY_LOGIN = "SELECT password FROM car_rent.customer WHERE login=?";
    private final static String SQL_CUSTOMER_PASSWORD = "password";
    private final static String SQL_FIND_CUSTOMER_BY_LOGIN = "SELECT customer.ID, user_roles.role_id FROM car_rent.customer INNER JOIN" +
            " user_roles ON customer.ID = user_roles.user_id WHERE customer.LOGIN = ?";
    private final static String SQL_GET_USER_EMAIL = "SELECT * FROM car_rent.customer WHERE EMAIL=?";
    private final static String SQL_GET_USER_LOGIN = "SELECT password FROM car_rent.customer WHERE login=?";
    private final static String SQL_SET_USER_TYPE = "UPDATE car_rent.customer SET TYPE=? WHERE login=?";
    private final static String SQL_GET_USER_TYPE = "SELECT TYPE FROM car_rent.customer WHERE id=?";
    private final static String SQL_SET_DESCRIPTION = "UPDATE car_rent.customer SET description =CONCAT(description,char(10), ?) WHERE login=?";
    private final static String SQL_GET_DESCRIPTION = "SELECT description FROM car_rent.customer WHERE login=?";
    private final static String SQL_LOGIN_BY_BOOKING_NUBMER = "SELECT login FROM car_rent.customer INNER JOIN booking ON " +
            "customer.id = booking.customer_id WHERE booking.booking_number = ?";
    private final static String SQL_EMAIL_BY_LOGIN = "SELECT EMAIL FROM car_rent.customer WHERE login=?";
    private final static String SQL_CREATE_USER = "INSERT INTO customer (login, EMAIL, first_name, surname, date_of_birth, PASSWORD)" +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private final static String NEUTRAL_TYPE = "neutral";
    private final static String SQL_USER_TYPE = "type";
    private final static String TRUST_TYPE = "trust";
    private final static String SQL_USER_LOGIN = "login";
    private final static String SQL_USER_EMAIL = "email";
    private static ConnectionFactory connectionFactory = new ConnectionFactory();
    private final static String SQL_USER_ID = "id";
    private final static String SQL_ROLE_ID = "role_id";
    private final static int SQL_ADMIN_TAG = 1;
    private final static String ADMIN_ROLE = "admin";
    private final static String USER_ROLE = "admin";


    @Override
    public void setType(String login, String type) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_SET_USER_TYPE);
            preparedStatement.setString(1, type);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();

        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in setType()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public boolean checkLogin(String login) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_LOGIN);
            preparedStatement.setString(1, login);
            ResultSet getLogin = preparedStatement.executeQuery();
            if (!getLogin.next()) {
                return true;
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in checkLogin()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return false;
    }

    @Override
    public boolean checkEmail(String email) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_EMAIL);
            preparedStatement.setString(1, email);
            ResultSet getEmail = preparedStatement.executeQuery();
            if (!getEmail.next())
                return true;
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in checkEmail()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return false;
    }

    @Override
    public Customer findCustomer(String login, String spassword) throws DAOException {
        Customer customer = null;
        String password = getPasswordByLogin(login);
        PasswordHasher passwordHasher = new PasswordHasher();
        if (password == null)
            return customer;
        else if (passwordHasher.validatePassword(spassword, password)) {
            Connection connection = null;
            PreparedStatement preparedStatement = null;
            try {
                connection = connectionFactory.getConnection();
                preparedStatement = connection.prepareStatement(SQL_FIND_CUSTOMER_BY_LOGIN);
                preparedStatement.setString(1, login);
                ResultSet rs = preparedStatement.executeQuery();
                rs.next();
                customer = new Customer();
                customer.setLogin(login);
                customer.setId(rs.getInt(SQL_USER_ID));
                int roleId = rs.getInt(SQL_ROLE_ID);
                if (roleId == SQL_ADMIN_TAG)
                    customer.setRole(ADMIN_ROLE);
                else
                    customer.setRole(USER_ROLE);
            } catch (SQLException | ConnectionFactoryException e) {
                throw new DAOException(e);
                // LOGGER.error("Error in findCustomer()", e);
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    throw new DAOException(e);
                }
            }
        } else
            return customer;
        return customer;
    }

    @Override
    public boolean checkTypeOfCustomer(int customerId) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_USER_TYPE);
            preparedStatement.setInt(1, customerId);
            ResultSet getType = preparedStatement.executeQuery();
            while (getType.next())
                if (getType.getString(SQL_USER_TYPE).equals(NEUTRAL_TYPE) || getType.getString(SQL_USER_TYPE).equals(TRUST_TYPE)) {
                    return true;
                }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("checkTypeOfCustomer", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }

        return false;
    }

    @Override
    public void setDescription(String login, String description) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_SET_DESCRIPTION);
            preparedStatement.setString(1, description);
            preparedStatement.setString(2, login);
            preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in setDescription()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public String getDescription(String login) throws DAOException {
        String description = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_DESCRIPTION);
            preparedStatement.setString(1, login);
            ResultSet getDescription = preparedStatement.executeQuery();
            while (getDescription.next()) {
                description = getDescription.getString("description");
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getDescription()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return description;
    }

    @Override
    public String getLoginByBookingNumber(int bookingNumber) throws DAOException {
        String login = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_LOGIN_BY_BOOKING_NUBMER);
            preparedStatement.setInt(1, bookingNumber);
            ResultSet getLogin = preparedStatement.executeQuery();
            getLogin.next();
            login = getLogin.getString(SQL_USER_LOGIN);
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getLoginByBookingNumber()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return login;
    }

    @Override
    public String getPasswordByLogin(String login) throws DAOException {
        String result = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_PASSWORD_BY_LOGIN);
            preparedStatement.setString(1, login);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next())
                result = rs.getString(SQL_CUSTOMER_PASSWORD);
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getPasswordByLogin", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return result;
    }

    @Override
    public String getEmailByLogin(String login) throws DAOException {
        String result = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_EMAIL_BY_LOGIN);
            preparedStatement.setString(1, login);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next())
                result =  rs.getString(SQL_USER_EMAIL);
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getEmailByLogin", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return result;
    }

    @Override
    public List<Customer> findAll() throws DAOException {
        throw new DAOException("Unsupported operation");
    }

    @Override
    public void create(Customer customer) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_USER);
            preparedStatement.setString(1, customer.getLogin());
            preparedStatement.setString(2, customer.getEmail());
            preparedStatement.setString(3, customer.getFirstName());
            preparedStatement.setString(4, customer.getSurname());
            preparedStatement.setString(6, customer.getPassword());
            preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in createCustomer()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public void delete(Integer id) throws DAOException {
        throw new DAOException("Unsupported operation");
    }

    @Override
    public Customer find(Integer id) throws DAOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void update(Customer customer) throws DAOException {
        throw new DAOException("Unsupported operation");
    }
}