package by.epam.trainings.task7.controller.command;

import by.epam.trainings.task7.dao.DAOException;

import javax.servlet.http.HttpServletRequest;

public interface ICommand {
    String execute(HttpServletRequest request) throws DAOException;
}
