package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:53
 * To change this template use File | Settings | File Templates.
 */
public class AllCarBookingsCommand implements ICommand {
    private static final String PARAM_NAME_CAR_ID = "carId";
    private static final String OPTION_BOOKING_PAGE = "path.page.option_bookings";
    private static final String MESSAGE = "message";
    private static final String BOOKINGS = "bookings";
    @Override
    public String execute(HttpServletRequest request) throws DAOException {
        String page = null;
        List<Booking> bookings;
        int carId = Integer.parseInt(request.getParameter(PARAM_NAME_CAR_ID));
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        bookings = bookingDAO.getAllForCar(carId);
        if (!bookings.isEmpty()) {
            request.getSession().setAttribute(BOOKINGS, bookings);
            page = ConfigurationManager.getProperty(OPTION_BOOKING_PAGE);
        } else {
            page = ConfigurationManager.getProperty(OPTION_BOOKING_PAGE);
            request.setAttribute(MESSAGE, MessageManager.HAVE_NOT_ORDERS);
        }
        return page;
    }
}
