package by.epam.trainings.task7.connectionFactory;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 18.02.16
 * Time: 0:36
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionFactoryException extends Exception {
    public ConnectionFactoryException() {
    }

    public ConnectionFactoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionFactoryException(Throwable cause) {
        super(cause);
    }

    protected ConnectionFactoryException(String message) {
        super(message);
    }
}
