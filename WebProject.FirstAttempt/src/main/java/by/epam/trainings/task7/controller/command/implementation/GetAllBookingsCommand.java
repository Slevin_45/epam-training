package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:52
 * To change this template use File | Settings | File Templates.
 */
public class GetAllBookingsCommand implements ICommand {
    private static final String BOOKINGS = "bookings";
    private static final String BOOKINGS_PAGE = "path.page.bookings";
    private static  final  String PARAM_NAME_USER_ID = "userId";
    private static final String MESSAGE = "message";
    private static final String ERROR_PAGE = "path.page.error";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Booking> bookingList = null;
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        int userId = (int) request.getSession().getAttribute(PARAM_NAME_USER_ID);
        try {
            bookingList = bookingDAO.getAllForUser(userId);
            request.setAttribute(BOOKINGS, bookingList);
            page = ConfigurationManager.getProperty(BOOKINGS_PAGE);
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
