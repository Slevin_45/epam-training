package by.epam.trainings.task7.controller.command.enumeration;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.controller.command.implementation.*;
import by.epam.trainings.task7.controller.command.implementation.accaunt.LoginCommand;
import by.epam.trainings.task7.controller.command.implementation.accaunt.LogoutCommand;
import by.epam.trainings.task7.controller.command.implementation.accaunt.RegisterCommand;
import by.epam.trainings.task7.controller.command.implementation.admin.*;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 16.02.16
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
public enum CommandEnum {
    GET_PROBLEMATIC_BOOKINGS {
        {
            this.command = new GetProblemBookingsCommand();
        }
    },
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    CONFIRM_BOOKING {
        {
            this.command = new ConfirmBookingCommand();
        }
    },
    CREAT_BOOKING {
        {
            this.command = new CreateBookingCommand();
        }
    },
    DELETE_BOOKING {
        {
            this.command = new DeleteBookingCommand();
        }
    },
    UPDATE_BOOKING {
        {
            this.command = new UpdateBookingCommand();
        }
    },
    SHOW_BOOKINGS {
        {
            this.command = new GetAllBookingsCommand();
        }
    },
    ADD_CAR {
        {
            this.command = new AddCarCommand();
        }
    },
    SHOW_CAR_BOOKINGS {
        {
            this.command = new AllCarBookingsCommand();
        }
    },
    CAR_DELETE {
        {
            this.command = new CarDeleteCommand();
        }
    },
    SHOW_CARS {
        {
            this.command = new GetAllCarsCommand();
        }
    },
    CAR_UPDATE {
        {
            this.command = new UpdateCarCommand();
        }
    },
    SHOW_CAR {
        {
            this.command = new GetCarCommand();
        }
    },
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    REGIST {
        {
            this.command = new RegisterCommand();
        }
    },
    CHECK_CAR {
        {
            this.command = new CheckCarCommand();
        }
    },
    GET_USER_DESCRIPT {
        {
            this.command = new GetUserDescriptCommand();
        }
    };


    ICommand command;

    public ICommand getCurrentCommand() {
        return command;
    }
}
