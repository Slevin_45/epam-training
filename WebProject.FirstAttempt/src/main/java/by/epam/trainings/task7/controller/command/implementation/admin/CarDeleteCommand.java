package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:53
 * To change this template use File | Settings | File Templates.
 */
public class CarDeleteCommand implements ICommand {
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";
    private static final String PARAM_NAME_CAR_ID = "carId";
    private static final String CARS = "cars";
    private static final String OPTION_CARS_PAGE = "path.page.option_cars";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        CarDAO carDAO = DAOFactory.getCarDAO();
        int carId = Integer.parseInt(request.getParameter(PARAM_NAME_CAR_ID));
        try {
            carDAO.delete(carId);
            List<Car> carList = carDAO.findAll();
            request.setAttribute(CARS, carList);
            page = ConfigurationManager.getProperty(OPTION_CARS_PAGE);
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
