package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 23:02
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
public final class Customer {

    private String email;
    private String firstName;
    private Integer id;
    private String login;
    private String password;
    private String surname;
    private String role;

    public Customer(String first_name, String surname, String login, String password, String email) {
        this.firstName = first_name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Customer() {

    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    private Set<CustomerPassport> customerPassports;
    private Set<Booking> bookings;

    @Column(name = "EMAIL", updatable = true, nullable = false, length = 45)
    public String getEmail() {
        return email;
    }

    @Column(name = "FIRST_NAME", updatable = true, nullable = true, length =
            45)
    public String getFirstName() {
        return firstName;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID", updatable = false, nullable = false)
    public Integer getId() {
        return id;
    }

    @Column(name = "LOGIN_NAME", updatable = false, nullable = false, length =
            45)
    public String getLogin() {
        return login;
    }

    @Column(name = "PASSWORD", updatable = true, nullable = false, length =
            45)
    public String getPassword() {
        return password;
    }


    @Column(name = "SURNAME", updatable = true, nullable = true, length = 45)
    public String getSurname() {
        return surname;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer", orphanRemoval = true, targetEntity = CustomerPassport.class)
    public Set<CustomerPassport> getCustomerPassports() {
        return customerPassports;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customer", orphanRemoval = true, targetEntity = Booking.class)
    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
