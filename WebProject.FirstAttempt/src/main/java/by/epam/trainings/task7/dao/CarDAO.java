package by.epam.trainings.task7.dao;

import by.epam.trainings.task7.beans.Car;

public interface CarDAO extends GenericDAO<Car, Integer> {

}
