package by.epam.trainings.task7.dao;

import by.epam.trainings.task7.beans.Customer;

public interface CustomerDAO extends GenericDAO<Customer, Integer> {
    public void setType(String login, String type) throws DAOException;
    public boolean checkLogin(String login) throws DAOException;
    public boolean checkEmail(String email) throws DAOException;
    public Customer findCustomer(String username, String password) throws DAOException;
    public boolean checkTypeOfCustomer(int customerId) throws DAOException;
    public void setDescription(String login, String description) throws DAOException;
    public String getDescription(String login) throws DAOException;
    public String getLoginByBookingNumber(int bookingNumber) throws DAOException;
    public String getPasswordByLogin(String login) throws DAOException;
    public String getEmailByLogin(String login) throws DAOException;
}
