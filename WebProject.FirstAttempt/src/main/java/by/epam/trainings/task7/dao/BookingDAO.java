package by.epam.trainings.task7.dao;

import by.epam.trainings.task7.beans.Booking;

import java.util.List;

public interface BookingDAO extends GenericDAO<Booking, Integer> {
    public List<Booking> getAllProblematicBooking() throws DAOException;
    public List<Booking> getAllForCar(int carId) throws DAOException;
    public boolean checkDate(int carId, String booking_date, String return_date) throws DAOException;
    public void confirmBooking(int bookingNumber) throws DAOException;
    public List<Booking> getAllForUser(int userId) throws DAOException;
}

