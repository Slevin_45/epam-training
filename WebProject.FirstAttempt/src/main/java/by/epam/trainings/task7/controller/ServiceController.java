package by.epam.trainings.task7.controller;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 16.02.16
 * Time: 22:21
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "ServiceController")
public class ServiceController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ServiceController.class);
    private static final long serialVersionUID = 1L;
    private final static ActionFactory actionFactory = new ActionFactory();
    private static final String INDEX_PAGE = "path.page.index";
    private static final String MESSAGE = "message";
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, DAOException {
        String page = null;
        ICommand command = actionFactory.defineCommand(request);
        try {
            page = command.execute(request);
        } catch (DAOException e) {
            throw new DAOException(e);
        }

        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty(INDEX_PAGE);
            request.getSession().setAttribute(MESSAGE,
                    MessageManager.NULL_PAGE);
            response.sendRedirect(request.getContextPath() + page);
        }
    }
}
