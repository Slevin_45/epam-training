package by.epam.trainings.task7.controller.command.implementation.accaunt;

import by.epam.trainings.task7.beans.Customer;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 16.02.16
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
public class LoginCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static final String INDEX_PAGE = "path.page.index";
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String LOGIN_PAGE = "path.page.login";
    private static final String MAIN_PAGE = "path.page.main";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";
    private static final String USER = "user";
    private static final String URL = "url";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        Customer customer = null;
        try {
            customer = DAOFactory.getCustomerDAO().findCustomer(login, password);
            if (customer == null) {
                request.setAttribute(MESSAGE, MessageManager.LOGIN_ERROR);
                page = ConfigurationManager.getProperty(LOGIN_PAGE);
            }
            else {
                page = ConfigurationManager.getProperty(INDEX_PAGE);
            }
        } catch (DAOException e) {
            //LOGGER.error("Error during user login: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }

        return page;
    }
    /*
    String login = req.getParameter("login");
        String password = req.getParameter("password");
        Customer customer = customerDao.findCustomer(login, password);
        if (customer != null) {
            if (customer.getRole().equals("admin")) {
                req.getSession().setAttribute("user", customer.getLogin());
                req.getSession().setAttribute("role", "admin");
                req.getSession().setAttribute("emailPassword", password);
                resp.sendRedirect("/pages/option_index.jsp");
            } else {
                req.getSession().setAttribute("user", customer.getLogin());
                req.getSession().setAttribute("userId", customer.getId());
                req.getSession().setAttribute("role", "user");
                req.getSession().setAttribute("language", "ru");
                resp.sendRedirect("/pages/index.jsp");
            }
        } else {
            req.setAttribute("error", "Unknown login, try again"); // Set error msg for ${error}
            req.getRequestDispatcher("/pages/login.jsp").forward(req, resp); // Go back to login page.
        }
     */
}
