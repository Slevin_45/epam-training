package by.epam.trainings.task7.dao.impl;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.connectionFactory.ConnectionFactory;
import by.epam.trainings.task7.connectionFactory.ConnectionFactoryException;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookingDAOImpl implements BookingDAO {
    private static final String SQL_CAR_ORDERS = "SELECT * FROM car_rent.booking INNER JOIN booking_status ON" +
            " booking.booking_number = booking_status.booking_id INNER JOIN customer ON customer.id = booking.customer_id WHERE booking.car_id = ?";
    private static final String SQL_CHECK_DATE = "SELECT booking_number FROM car_rent.booking WHERE EXISTS" +
            " (SELECT * FROM booking WHERE car_id = ? AND ? <= RETURN_DATE AND ? >= BOOKING_DATE) AND car_id = ?";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String SQL_INSERT_BOOKING = "INSERT INTO car_rent.booking (customer_id, car_id, booking_date, return_date)" +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_INSERT_PASSPORT_DATA = "INSERT INTO car_rent.customer_pass_info (NUMBER, date_of_issue, pass_expiry, customer_id) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_DELETE_BOOKING = "DELETE FROM car_rent.booking WHERE booking_number=?;";
    private static final String SQL_USER_BOOKINGS = "SELECT * FROM car_rent.booking INNER JOIN booking_status ON" +
            " booking.booking_number = booking_status.booking_id WHERE booking.customer_id = ?";
    private static final String SQL_UPDATE_BOOKING = "UPDATE booking SET booking_date=?, return_date=? WHERE booking_number=?;";
    private static final String SQL_CONFIRM_BOOKING = "UPDATE booking_status SET status_name = 'Approved' WHERE booking_id = ?";
    private static final String SQL_GET_PROBLEMATIC_BOOKINGS = "SELECT * FROM car_rent.booking INNER JOIN booking_status ON " +
            "booking.booking_number = booking_status.booking_id INNER JOIN car_rent.customer ON customer.id = booking.customer_id WHERE customer.TYPE = ?";
    private static final String SQL_CREATE_ORDER = "INSERT INTO store.orders(user_id, order_date) VALUES (?, ?)";
    private static final String SQL_ADD_PRODUCT_TO_ORDER = "INSERT INTO store.orders_products(order_id, product_id, amount) VALUES (?, ?, ?)";
    private static final String SQL_DELETE_ORDER = "DELETE w,e FROM store.orders w INNER JOIN store.orders_products e ON w.order_id=e.order_id WHERE e.order_id=?";
    private static final String SQL_ORDERS = "SELECT * FROM store.orders JOIN store.orders_products ON orders.order_id=orders_products.order_id JOIN store.products ON orders_products.product_id=products.product_id JOIN store.categories ON products.category=categories.category_id";
    private static final String SQL_PRODUCTS_FROM_ORDER = "SELECT * FROM store.orders_products JOIN store.products ON orders_products.product_id=products.product_id JOIN store.categories ON products.category=categories.category_id WHERE order_id=?";
    private static final String SQL_DELETE_PRODUCT = "DELETE FROM store.orders_product WHERE order_id=?";
    private static final String SQL_ORDER_BY_ID = "SELECT * FROM store.orders WHERE user_id=?";
    private static final String SQL_ORDER_ID = "order_id";
    private static final String SQL_USER_ID = "user_id";
    private static final String SQL_PRODUCT_AMOUNT_IN_ORDER = "orders_products.amount";
    private static final String SQL_ORDER_DATE = "order_date";
    private static final String SQL_PRODUCT_ID = "product_id";
    private static final String SQL_PRODUCT_NAME = "product_name";
    private static final String SQL_PRICE = "price";
    private static final String SQL_DESCRIPTION = "description";
    private static final String SQL_CATEGORY_NAME = "name";
    private static final String SQL_USER_LOGIN = "login";
    private static final String SQL_CAR_ID = "car_id";
    private static final String SQL_BOOKING_STATUS = "status_name";
    private static final String SQL_BOOKING_NUMBER = "booking_Number";
    private static final String SQL_BOOKING_DATE = "booking_Date";
    private static final String SQL_RETURN_DATE = "return_Date";
    private static final String NOT_TRUST = "not trust";
    private static ConnectionFactory connectionFactory = new ConnectionFactory();


    @Override
    public List<Booking> getAllProblematicBooking() throws DAOException {
        List<Booking> bookingList = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_PROBLEMATIC_BOOKINGS);
            preparedStatement.setString(1, NOT_TRUST);
            ResultSet rs = preparedStatement.executeQuery();
            bookingList = new ArrayList<>();
            while (rs.next()) {
                Booking booking = new Booking();
                booking.setCustomer(rs.getString(SQL_USER_LOGIN));
                booking.setCarId(rs.getInt(SQL_CAR_ID));
                booking.setStatus(rs.getString(SQL_BOOKING_STATUS));
                booking.setBookingNumber(rs.getInt(SQL_BOOKING_NUMBER));
                booking.setBookingDate(rs.getString(SQL_BOOKING_DATE));
                booking.setReturnDate(rs.getString(SQL_RETURN_DATE));
                bookingList.add(booking);
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getProblematicBooking() :", e);
        }
        return bookingList;
    }

    @Override
    public List<Booking> getAllForUser(int userId) throws DAOException {
        Connection connection = null;
        PreparedStatement stm = null;
        List<Booking> bookingList;
        try {
            connection = connectionFactory.getConnection();
            stm = connection.prepareStatement(SQL_USER_BOOKINGS);
            stm.setInt(1, userId);
            ResultSet rs = stm.executeQuery();
            bookingList = new ArrayList<Booking>();
            while (rs.next()) {
                Booking booking = new Booking();
                booking.setCarId(rs.getInt(SQL_CAR_ID));
                booking.setStatus(rs.getString(SQL_BOOKING_STATUS));
                booking.setBookingNumber(rs.getInt(SQL_BOOKING_NUMBER));
                booking.setBookingDate(rs.getString(SQL_BOOKING_DATE));
                booking.setReturnDate(rs.getString(SQL_RETURN_DATE));
                bookingList.add(booking);
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            // LOGGER.error("Error in getAllForUser()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return bookingList;
    }

    @Override
    public List<Booking> getAllForCar(int carId) throws DAOException {
        List<Booking> bookingList = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CAR_ORDERS);
            preparedStatement.setInt(1, carId);
            ResultSet rs = preparedStatement.executeQuery();
            bookingList = new ArrayList<>();
            while (rs.next()) {
                Booking booking = new Booking();
                booking.setCustomer(rs.getString(SQL_USER_LOGIN));
                booking.setCarId(rs.getInt(SQL_CAR_ID));
                booking.setStatus(rs.getString(SQL_BOOKING_STATUS));
                booking.setBookingNumber(rs.getInt(SQL_BOOKING_NUMBER));
                booking.setBookingDate(rs.getString(SQL_BOOKING_DATE));
                booking.setReturnDate(rs.getString(SQL_RETURN_DATE));
                bookingList.add(booking);
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getAllForCar()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return bookingList;
    }

    @Override
    public boolean checkDate(int carId, String sbooking_date, String sreturn_date) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Date booking_date = null;
        Date return_date = null;
        if (booking_date.compareTo(return_date) > 0) {
            return false;
        }
        try {
            booking_date = new SimpleDateFormat(DATE_FORMAT).parse(sbooking_date);
            return_date = new SimpleDateFormat(DATE_FORMAT).parse(sreturn_date);
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CHECK_DATE);
            preparedStatement.setInt(1, carId);
            preparedStatement.setDate(2, new java.sql.Date(booking_date.getTime()));
            preparedStatement.setDate(3, new java.sql.Date(return_date.getTime()));
            preparedStatement.setInt(4, carId);
            ResultSet getDate = preparedStatement.executeQuery();
            if (!getDate.next()) {
                return true;
            }
        } catch (SQLException | ParseException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in checkDate()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return false;
    }

    @Override
    public void confirmBooking(int bookingNumber) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_CONFIRM_BOOKING);
            preparedStatement.setInt(1, bookingNumber);
            preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in confirmBooking()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public List<Booking> findAll() throws DAOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void create(Booking booking) throws DAOException {
        Connection connection = null;
        PreparedStatement insertBookingStm = null;
        PreparedStatement insertPassportInfoStm = null;
        Date booking_date = null;
        Date return_date = null;
        Date date_of_issue = null;
        Date pass_expiry = null;
        try {
            booking_date = new SimpleDateFormat(DATE_FORMAT).parse(booking.getBookingDate());
            return_date = new SimpleDateFormat(DATE_FORMAT).parse(booking.getReturnDate());
            date_of_issue = new SimpleDateFormat(DATE_FORMAT).parse(booking.getCustomerPass_date_of_issue());
            pass_expiry = new SimpleDateFormat(DATE_FORMAT).parse(booking.getCustomerPass_expiry());
            connection = connectionFactory.getConnection();
            connection.setAutoCommit(false);
            insertBookingStm = connection.prepareStatement(SQL_INSERT_BOOKING);
            insertPassportInfoStm = connection.prepareStatement(SQL_INSERT_PASSPORT_DATA);

            insertBookingStm.setInt(1, booking.getCustomerId());
            insertBookingStm.setInt(2, booking.getCarId());
            insertBookingStm.setDate(3, new java.sql.Date(booking_date.getTime()));
            insertBookingStm.setDate(4, new java.sql.Date(return_date.getTime()));
            insertBookingStm.executeUpdate();

            insertPassportInfoStm.setString(1, booking.getCustomerPass_number());
            insertPassportInfoStm.setDate(2, new java.sql.Date(date_of_issue.getTime()));
            insertPassportInfoStm.setDate(3, new java.sql.Date(pass_expiry.getTime()));
            insertPassportInfoStm.setInt(4, booking.getCustomerId());
            insertPassportInfoStm.executeUpdate();
            connection.commit();

        } catch (SQLException | ParseException | ConnectionFactoryException e) {
            try {
                connection.rollback();
            } catch (SQLException excep) {
                throw new DAOException(e);
            }
        } finally {
            try {
                connection.setAutoCommit(true);
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public void delete(Integer id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_BOOKING);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in delete()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public Booking find(Integer id) throws DAOException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void update(Booking booking) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Date bookingDate = null;
        Date returnDate = null;
        try {
            bookingDate = new SimpleDateFormat(DATE_FORMAT).parse(booking.getBookingDate());
            returnDate = new SimpleDateFormat(DATE_FORMAT).parse(booking.getReturnDate());
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_BOOKING);
            preparedStatement.setDate(1, new java.sql.Date(bookingDate.getTime()));
            preparedStatement.setDate(2, new java.sql.Date(returnDate.getTime()));
            preparedStatement.setInt(3, booking.getBookingNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException | ParseException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in update()", e);
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }
}
