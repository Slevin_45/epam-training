package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CustomerDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:55
 * To change this template use File | Settings | File Templates.
 */
public class GetUserDescriptCommand implements ICommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String DESCRIPTION = "description";
    private static final String MESSAGE = "message";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String USER_DESCRIPTION_PAGE = "path.page.user_description";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String description = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        CustomerDAO customerDAO = DAOFactory.getCustomerDAO();
        try {
            description = customerDAO.getDescription(login);
            request.getSession().setAttribute(DESCRIPTION, description);
            page = ConfigurationManager.getProperty(USER_DESCRIPTION_PAGE);
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
