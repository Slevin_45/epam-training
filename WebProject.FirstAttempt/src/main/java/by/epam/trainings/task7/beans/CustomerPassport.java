package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 23:08
 * To change this template use File | Settings | File Templates.
 */

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CUSTOMER_PASS_INFO")
public class CustomerPassport {

    private Integer number;
    private Integer id;
    private Date date_of_issue;
    private Date pass_expiry;
    private Customer customer;

    @Column(name = "NUMBER", updatable = true, nullable = false)
    public Integer getNumber() {
        return number;
    }

    @Column(name = "DATE_OF_ISSUE", updatable = true, nullable = false)
    public Date getDate_of_issue() {
        return date_of_issue;
    }

    @Id
    @GeneratedValue
    @Column(name = "ID", updatable = false, nullable = false)
    public Integer getId() {
        return id;
    }

    @Column(name = "PASS_EXPIRY", updatable = true, nullable = false)
    public Date getPass_expiry() {
        return pass_expiry;
    }

    @ManyToOne(optional = false, targetEntity = Customer.class)
    @JoinColumn(name = "CUSTOMER_ID", updatable = false, nullable = false, referencedColumnName = "ID")
    public Customer getCustomer() {
        return customer;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setDate_of_issue(Date date_of_issue) {
        this.date_of_issue = date_of_issue;
    }

    public void setPass_expiry(Date pass_expiry) {
        this.pass_expiry = pass_expiry;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
