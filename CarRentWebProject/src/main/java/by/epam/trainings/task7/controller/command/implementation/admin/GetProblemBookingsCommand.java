package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 19.02.16
 * Time: 11:58
 * To change this template use File | Settings | File Templates.
 */
public class GetProblemBookingsCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(GetProblemBookingsCommand.class);
    private static final String BOOKINGS = "bookings";
    private static final String OPTION_BOOKINGS_PAGE = "path.page.option_booking";
    private static final String PARAM_NAME_USER_ID = "userId";
    private static final String MESSAGE = "message";
    private static final String ERROR_PAGE = "path.page.error";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Booking> bookingList = null;
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        try {
            bookingList = bookingDAO.getAllProblematicBooking();
            request.setAttribute(BOOKINGS, bookingList);
            page = ConfigurationManager.getProperty(OPTION_BOOKINGS_PAGE);
        } catch (DAOException e) {
            LOGGER.error("Error during get problem bookings: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }

        return page;
    }
}
