package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 22:59
 * To change this template use File | Settings | File Templates.
 */

public final class Booking {
    private String bookingDate;
    private int bookingNumber;
    private String returnDate;
    private String status;
    private String customerLogin;
    private int carId;
    private int customerId;
    private CustomerPassport customerPassport;


    public Booking() {

    }

    public Booking(CustomerPassport customerPassport, int carId, String booking_date, String return_date) {
        this.customerPassport = customerPassport;
        this.carId = carId;
        this.bookingDate = booking_date;
        this.returnDate = return_date;
    }
    /**
     * This method returns object of class CustomerPassport
     * @return CustomerPassport object of CustomerPassport .
     */
    public CustomerPassport getCustomerPassport() {
        return customerPassport;
    }
    /**
     * This method sets customer passport
     * @param customerPassport object of customerPassport class
     */
    public void setCustomerPassport(CustomerPassport customerPassport) {
        this.customerPassport = customerPassport;
    }
    /**
     * This method returns customer id
     * @return int returns customerId
     */
    public int getCustomerId() {
        return customerId;
    }
    /**
     * This method is used to set two customer id.
     * @param customerId This is the first parameter to addNum method
     */
    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    /**
     * This method is used to set status.
     * @param status this is status of booking
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * This method is used to get status.
     * @return  String returns status
     */
    public String getStatus() {
        return status;
    }

    /**
     * This method is used to get booking date.
     * @return String this is booking date
     */
    public String getBookingDate() {
        return bookingDate;
    }

    /**
     * This method is used to get booking date.
     * @return String this is booking date
     */
    public int getBookingNumber() {
        return bookingNumber;
    }
    /**
     * This method is used to get return date.
     * @return String this is return date
     */
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * This method is used to get customer login.
     * @return String this is customer login
     */
    public String getCustomerLogin() {
        return customerLogin;
    }

    /**
     * This method is used to get car id.
     * @return int this is a car id
     */
    public int getCarId() {
        return carId;
    }
    /**
     * This method is used to set booking date.
     * @param  bookingDate this is booking date
     */
    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }
    /**
     * This method is used to set booking number.
     * @param  bookingNumber this is booking number
     */
    public void setBookingNumber(int bookingNumber) {
        this.bookingNumber = bookingNumber;
    }
    /**
     * This method is used to set return date.
     * @param  returnDate this is return date
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
    /**
     * This method is used to set customer.
     * @param  customerLogin this is customer login
     */
    public void setCustomerLogin(String customerLogin) {
        this.customerLogin = customerLogin;
    }
    /**
     * This method is used to set car id.
     * @param  carId this is car id
     */
    public void setCarId(int carId) {
        this.carId = carId;
    }

}

