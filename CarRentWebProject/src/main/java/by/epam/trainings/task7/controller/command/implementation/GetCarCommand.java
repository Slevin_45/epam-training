package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:54
 * To change this template use File | Settings | File Templates.
 */
public class GetCarCommand implements ICommand {
    private static final String PARAM_NAME_CARID = "carId";
    private static final String MESSAGE = "message";
    private static final String CAR_ID = "carId";
    private static final String CAR = "car";
    private static final String ROLE = "role";
    private static final String USER_ROLE = "user";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String CAR_PAGE = "path.page.car_page";
    private static final String CARS_PAGE = "path.page.cars";
    private static final String OPTION_CAR_PAGE = "path.page.option_car";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        int carId = Integer.parseInt(request.getParameter(PARAM_NAME_CARID));
        request.getSession().setAttribute(CAR_ID, carId);
        CarDAO carDAO = DAOFactory.getCarDAO();
        try {
            Car car = carDAO.find(carId);
            if (car != null) {
                request.getSession().setAttribute(CAR, car);
                if (request.getSession().getAttribute(ROLE) == null || request.getSession().getAttribute(ROLE).equals(USER_ROLE))
                page = ConfigurationManager.getProperty(CAR_PAGE);
                else
                    page = ConfigurationManager.getProperty(OPTION_CAR_PAGE);
            }
            else{
                request.setAttribute(MESSAGE, MessageManager.CAR_NOT_FOUND);
                page = ConfigurationManager.getProperty(CARS_PAGE);
            }
        } catch (DAOException e) {
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
