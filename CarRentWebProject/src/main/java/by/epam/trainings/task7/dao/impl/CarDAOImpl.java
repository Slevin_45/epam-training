package by.epam.trainings.task7.dao.impl;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.beans.CarType;
import by.epam.trainings.task7.connectionFactory.ConnectionFactory;
import by.epam.trainings.task7.connectionFactory.ConnectionFactoryException;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CarDAOImpl implements CarDAO {
    private static final String SQL_GET_ALL_CARS = "SELECT car_type.model, car_type.producer, car.id, car.base_price_per_day," +
            " car.image_src FROM car_rent.car_type INNER JOIN car ON car_type.id = car.car_type_id";
    private static final String SQL_GET_CAR = "SELECT * FROM car_rent.car_type INNER JOIN car ON car_type.id = car.car_type_id" +
            " WHERE car.id = ?";
    private static final String SQL_INSERT_CAR_TYPE = "INSERT INTO car_type (model, producer, transmission) VALUES (?, ?, ?)";
    private static final String SQL_INSERT_CAR = "INSERT INTO car_rent.car (registration_number, date_of_manufacturing, base_price_per_day," +
            " car_type_id, image_src) SELECT ?, ?, ?, MAX(car_type.id), ? FROM car_rent.car_type";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String SQL_DELETE_CAR = "DELETE FROM car_type WHERE id=?";
    private static final String SQL_UPDATE_CAR_TYPE = "UPDATE car_type SET car_type.model = ?, car_type.producer = ?," +
            " car_type.transmission = ? WHERE id = ? ";
    private static final String SQL_UPDATE_CAR = "UPDATE car SET car.registration_number = ?, car.date_of_manufacturing = ?," +
            " car.base_price_per_day = ? WHERE id = ?";
    private static final String SQL_CAR_PRODUCER = "producer";
    private static final String SQL_CAR_ID = "id";
    private static final String SQL_CAR_PRICE = "base_price_per_day";
    private static final String SQL_CAR_IMAGE = "image_src";
    private static final String SQL_CAR_TRANSMISSION = "transmission";
    private static final String SQL_CAR_DATE_OF_MANUFACTURING = "date_of_manufacturing";
    private static final String SQL_CAR_REGIST_NUMBER = "registration_number";
    private static final String SQL_CAR_MODEL = "model";
    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public List<Car> findAll() throws DAOException {
        List<Car> carList = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_ALL_CARS);
            ResultSet rs = preparedStatement.executeQuery();
            carList = new ArrayList<>();
            while (rs.next()) {
                CarType carType = new CarType();
                carType.setModel(rs.getString(SQL_CAR_MODEL));
                carType.setModel(rs.getString(SQL_CAR_PRODUCER));
                Car car = new Car();
                car.setCarType(carType);
                car.setId(rs.getInt(SQL_CAR_ID));
                car.setBasePricePerDay(rs.getDouble(SQL_CAR_PRICE));
                car.setImage_src(rs.getString(SQL_CAR_IMAGE));
                carList.add(car);
            }
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error getAll()", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return carList;
    }

    @Override
    public void create(Car car) throws DAOException {
        Connection connection = null;
        PreparedStatement insertCarStm = null;
        PreparedStatement insertCarTypeStm = null;
        Date date_of_manufacturing = null;
        String t = car.getDateOfManufacturing();
        try {
            date_of_manufacturing = new SimpleDateFormat(DATE_FORMAT).parse(car.getDateOfManufacturing());
            connection = connectionFactory.getConnection();
            insertCarTypeStm = connection.prepareStatement(SQL_INSERT_CAR_TYPE);
            insertCarTypeStm.setString(1, car.getCarType().getModel());
            insertCarTypeStm.setString(2, car.getCarType().getProducer());
            insertCarTypeStm.setString(3, car.getCarType().getTransmission());
            insertCarTypeStm.executeUpdate();

            insertCarStm = connection.prepareStatement(SQL_INSERT_CAR);
            insertCarStm.setString(1, car.getRegistrationNumber());
            insertCarStm.setDate(2, new java.sql.Date(date_of_manufacturing.getTime()));
            insertCarStm.setDouble(3, car.getBasePricePerDay());
            insertCarStm.setString(4, car.getImage_src());
            insertCarStm.executeUpdate();
        } catch (SQLException | ParseException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in create()", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public void delete(Integer id) throws DAOException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_DELETE_CAR);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in deleteCar()", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
    }

    @Override
    public Car find(Integer id) throws DAOException {
        Car car = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = connectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(SQL_GET_CAR);
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            rs.next();
            CarType carType = new CarType();
            carType.setId(rs.getInt(SQL_CAR_ID));
            carType.setModel(rs.getString(SQL_CAR_MODEL));
            carType.setProducer(rs.getString(SQL_CAR_PRODUCER));
            carType.setTransmission(rs.getString(SQL_CAR_TRANSMISSION));
            car = new Car();
            car.setId(rs.getInt(SQL_CAR_ID));
            car.setBasePricePerDay(rs.getDouble(SQL_CAR_PRICE));
            car.setDateOfManufacturing(rs.getString(SQL_CAR_DATE_OF_MANUFACTURING));
            car.setRegistrationNumber(rs.getString(SQL_CAR_REGIST_NUMBER));
            car.setImage_src(rs.getString(SQL_CAR_IMAGE));
            car.setCarType(carType);
        } catch (SQLException | ConnectionFactoryException e) {
            throw new DAOException(e);
            //LOGGER.error("Error in getCar()", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                throw new DAOException(e);
            }
        }
        return car;
    }

    @Override
    public void update(Car car) throws DAOException {
        Connection connection = null;
        PreparedStatement updateCarTypeStm = null;
        PreparedStatement updateCarStm = null;
        Date date_of_manufacturing = null;
        try {
            date_of_manufacturing = new SimpleDateFormat(DATE_FORMAT).parse(car.getDateOfManufacturing());
            connection = connectionFactory.getConnection();
            connection.setAutoCommit(false);
            updateCarTypeStm = connection.prepareStatement(SQL_UPDATE_CAR_TYPE);
            updateCarStm = connection.prepareStatement(SQL_UPDATE_CAR);

            updateCarTypeStm.setString(1, car.getCarType().getModel());
            updateCarTypeStm.setString(2, car.getCarType().getProducer());
            updateCarTypeStm.setString(3, car.getCarType().getTransmission());
            updateCarTypeStm.setInt(4, car.getId());
            updateCarTypeStm.executeUpdate();

            updateCarStm.setString(1, car.getRegistrationNumber());
            updateCarStm.setDate(2, new java.sql.Date(date_of_manufacturing.getTime()));
            updateCarStm.setDouble(3, car.getBasePricePerDay());
            updateCarStm.setInt(4, car.getId());
            updateCarStm.executeUpdate();

            connection.commit();

        } catch (SQLException | ParseException | ConnectionFactoryException e) {
            //LOGGER.error("Error in updateCar()", e);
            try {
                //LOGGER.info("Transaction is being rolled back");
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException excep) {
                throw new DAOException(e); //LOGGING!!!
            }
        } finally {
            try {
                if (connection != null) {
                    connection.setAutoCommit(true);
                }
                connection.close();
            } catch (SQLException e) {
                throw new DAOException(e);
                //LOGGER.error("Transaction rollBack error", e);
            }
        }
    }
}
