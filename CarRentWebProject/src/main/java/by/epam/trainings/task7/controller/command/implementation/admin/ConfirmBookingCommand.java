package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
public class ConfirmBookingCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(ConfirmBookingCommand.class);
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";
    private static final String PARAM_NAME_BOOKING_NUMBER = "bookingNumber";
    private static final String OPTION_BOOKINGS_PAGE = "path.page.option_booking";
    private static final String BOOKINGS = "bookings";

    @Override
    public String execute(HttpServletRequest request) {
        List<Booking> bookingList;
        String page = null;
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        int bookingNumber = Integer.parseInt(request.getParameter(PARAM_NAME_BOOKING_NUMBER));
        try {
            bookingDAO.confirmBooking(bookingNumber);
            bookingList = bookingDAO.getAllProblematicBooking();
            request.setAttribute(BOOKINGS, bookingList);
            request.setAttribute(MESSAGE, MessageManager.BOOKING_APPROVED);
            page = ConfigurationManager.getProperty(OPTION_BOOKINGS_PAGE);
        } catch (DAOException e) {
            LOGGER.error("Error during confirm booking: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
