package by.epam.trainings.task7.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 31.01.16
 * Time: 20:35
 * To change this template use File | Settings | File Templates.
 */
public class AdminFilter implements Filter {
    private static final String ROLE = "role";
    private static final String ADMIN_ROLE = "admin";
    private static final String ERROR_PAGE = "path.page.error";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (((HttpServletRequest) servletRequest).getSession().getAttribute(ROLE).equals(ADMIN_ROLE)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse) servletResponse).sendRedirect(ERROR_PAGE);
        }
    }

    @Override
    public void destroy() {

    }
}
