package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 23:02
 * To change this template use File | Settings | File Templates.
 */

public final class Customer {

    private String email;
    private String firstName;
    private int id;
    private String login;
    private String password;
    private String surname;
    private String role;

    public Customer(String first_name, String surname, String login, String password, String email) {
        this.firstName = first_name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public Customer() {

    }

    public Customer(int customerId) {
        this.id = customerId;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getSurname() {
        return surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
