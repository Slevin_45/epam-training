package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 22:59
 * To change this template use File | Settings | File Templates.
 */


public final class CarType {
    private String transmission;
    private int id;
    private String model;
    private String producer;

    public CarType(String model, String producer, String transmission) {
        this.model = model;
        this.producer = producer;
        this.transmission = transmission;
    }

    public CarType() {

    }


    public String getTransmission() {
        return transmission;
    }


    public int getId() {
        return id;
    }


    public String getModel() {
        return model;
    }

    public String getProducer() {
        return producer;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }
}
