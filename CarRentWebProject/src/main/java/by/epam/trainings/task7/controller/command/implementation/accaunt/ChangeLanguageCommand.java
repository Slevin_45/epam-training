package by.epam.trainings.task7.controller.command.implementation.accaunt;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:54
 * To change this template use File | Settings | File Templates.
 */
public class ChangeLanguageCommand implements ICommand {
    private String page;
    private static final String LOCALE = "locale";
    private static final String PARAM_NAME_LANGUAGE = "language";
    private static final String ROLE = "role";
    private static final String USER_ROLE = "user";
    private static final String INDEX_PAGE = "path.page.index";
    private static final String OPTION_INDEX_PAGE = "path.page.option_index";

    @Override
    public String execute(HttpServletRequest request) {
        String lang = request.getParameter(PARAM_NAME_LANGUAGE);
        request.getSession().setAttribute(LOCALE, new Locale(lang));
        if (request.getSession().getAttribute(ROLE) == null || request.getSession().getAttribute(ROLE).equals(USER_ROLE)) {
            page = ConfigurationManager.getProperty(INDEX_PAGE);
        } else {
            page = ConfigurationManager.getProperty(OPTION_INDEX_PAGE);
        }
        return page;
    }
}
