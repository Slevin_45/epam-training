package by.epam.trainings.task7.controller.command.implementation.accaunt;

import by.epam.trainings.task7.beans.Customer;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CustomerDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import by.epam.trainings.task7.passwordLogic.PasswordHasher;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:55
 * To change this template use File | Settings | File Templates.
 */
public class SignUpCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(SignUpCommand.class);
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String PARAM_NAME_FIRST_NAME = "first_name";
    private static final String PARAM_NAME_LAST_NAME = "last_name";
    private static final String PARAM_NAME_EMAIL = "email";
    private static final String SIGN_UP_PAGE = "path.page.signup";
    private static final String INDEX_PAGE = "path.page.index";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";

    @Override
    public String execute(HttpServletRequest request) {
        PasswordHasher passwordHasher = new PasswordHasher();
        String page = null;
        String first_name = request.getParameter(PARAM_NAME_FIRST_NAME).trim();
        String surname = request.getParameter(PARAM_NAME_LAST_NAME).trim();
        String email = request.getParameter(PARAM_NAME_EMAIL).trim();
        String login = request.getParameter(PARAM_NAME_LOGIN).trim();
        String spassword = request.getParameter(PARAM_NAME_PASSWORD).trim();
        String password = passwordHasher.createHash(spassword);
        Customer customer = new Customer(first_name, surname, login, password, email);
        CustomerDAO customerDAO = DAOFactory.getCustomerDAO();
        try {
            if (customerDAO.checkLogin(login))
                if (customerDAO.checkEmail(email)) {
                    customerDAO.create(customer);
                    request.setAttribute(MESSAGE, MessageManager.SIGN_UP_SUCCESS);
                    page = ConfigurationManager.getProperty(INDEX_PAGE);
                } else {
                    request.setAttribute(MESSAGE, MessageManager.INCORRECT_EMAIL);
                    page = ConfigurationManager.getProperty(SIGN_UP_PAGE);
                }
            else
            {
                request.setAttribute(MESSAGE, MessageManager.INCORRECT_LOGIN);
                page = ConfigurationManager.getProperty(SIGN_UP_PAGE);
            }
        } catch (DAOException e) {
            LOGGER.error("Error during user signup: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
