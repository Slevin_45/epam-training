package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.beans.CarType;
import by.epam.trainings.task7.connectionFactory.ConnectionFactory;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:53
 * To change this template use File | Settings | File Templates.
 */
public class UpdateCarCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(UpdateCarCommand.class);
    private static final String PARAM_NAME_CAR_PRODUCER = "producer";
    private static final String PARAM_NAME_CAR_ID = "carId";
    private static final String PARAM_NAME_CAR_PRICE = "base_price_per_day";
    private static final String PARAM_NAME_CAR_IMAGE = "image_src";
    private static final String PARAM_NAME_CAR_TRANSMISSION = "transmission";
    private static final String PARAM_NAME_CAR_DATE_OF_MANUFACTURING = "date_of_manufacturing";
    private static final String PARAM_NAME_CAR_REGIST_NUMBER = "registration_number";
    private static final String PARAM_NAME_CAR_MODEL = "model";
    private static final String OPTION_CARS_PAGE = "path.page.option_cars";
    private static final String MESSAGE = "message";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String CARS = "cars";
    private static ConnectionFactory connectionFactory = new ConnectionFactory();

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Car> carList = null;
        String model = request.getParameter(PARAM_NAME_CAR_MODEL);
        String producer = request.getParameter(PARAM_NAME_CAR_PRODUCER);
        String transmission = request.getParameter(PARAM_NAME_CAR_TRANSMISSION);
        String registNumber = request.getParameter(PARAM_NAME_CAR_REGIST_NUMBER);
        String dateOfManufacturing = request.getParameter(PARAM_NAME_CAR_DATE_OF_MANUFACTURING);
        double price = Double.parseDouble(request.getParameter(PARAM_NAME_CAR_PRICE));
        int carId = (Integer) request.getSession().getAttribute(PARAM_NAME_CAR_ID);
        CarType carType = new CarType(model, producer, transmission);
        Car car = new Car(carType, carId, registNumber, dateOfManufacturing, price);
        CarDAO carDao = DAOFactory.getCarDAO();
        try {
            carDao.update(car);
            carList = carDao.findAll();
            request.setAttribute(CARS, carList);
            request.setAttribute(MESSAGE, MessageManager.CAR_UPDATE);
            page = ConfigurationManager.getProperty(OPTION_CARS_PAGE);

        } catch (DAOException e) {
            LOGGER.error("Error during update car: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }

}
