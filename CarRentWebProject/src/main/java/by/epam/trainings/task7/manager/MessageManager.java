package by.epam.trainings.task7.manager;

public class MessageManager {
    public static final String REGIST_CAR_ERROR = "message.regist_car.error";
    public static final String LOGIN_ERROR = "message.login_error";
    public static final String NULL_PAGE = "message.null_page";
    public static final String BOOKING_SUCCESS = "message.booking.success";
    public static final String BOOKING_SUCCESS_BUT_WAIT = "message.booking.success_but_wait";
    public static final String INCORRECT_DATE = "message.incorrect_date";
    public static final String CAR_DELETE = "message.car_delete";
    public static final String CAR_UPDATE = "message.car_update";
    public static final String BOOKING_APPROVED = "message.booking_approved";
    public static final String DATABASE_ERROR = "message.database_error";
    public static final String CAR_NOT_FOUND = "message.car_not_found_start";
    public static final String SIGN_UP_SUCCESS = "message.sign_up.success";
    public static final String BOOKING_UPDATE_SUCCESS = "message.booking_update_success";
    public static final String BOOKING_UPDATE_ERROR = "message.booking_update_error";
    public static final String BOOKING_DELETE = "message.booking_delete";
    public static final String INCORRECT_LOGIN_OR_EMAIL = "message.login_incorrect";
    public static final String INCORRECT_EMAIL = "message.incorrect_email";
    public static final String INCORRECT_LOGIN = "message.incorrect_login";
    public static final String CAR_ADDED = "message.car_added";
}
