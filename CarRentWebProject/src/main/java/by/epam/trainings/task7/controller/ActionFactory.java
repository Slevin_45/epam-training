package by.epam.trainings.task7.controller;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.controller.command.enumeration.CommandEnum;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 16.02.16
 * Time: 22:13
 * To change this template use File | Settings | File Templates.
 */
public class ActionFactory {
    private static final String COMMAND = "command";

    public ICommand defineCommand(HttpServletRequest request) throws IllegalArgumentException {
        ICommand command = null;

        String action = request.getParameter(COMMAND);

        if (action == null || action.isEmpty()) {
            return command;
        }

        try {
            CommandEnum commandEnum = CommandEnum.valueOf(action.toUpperCase());
            command = commandEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e);
        }
        return command;
    }
}
