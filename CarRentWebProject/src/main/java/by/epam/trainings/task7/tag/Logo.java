package by.epam.trainings.task7.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 20.02.16
 * Time: 19:39
 * To change this template use File | Settings | File Templates.
 */
public class Logo  extends TagSupport {
    private static final String LOGO = "Elcars";

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().print(LOGO);
        } catch(IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        }
        return SKIP_BODY;
    }
}
