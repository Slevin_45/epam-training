package by.epam.trainings.task7.beans;




public final class Car {
    private String image_src;
    private double basePricePerDay;
    private String dateOfManufacturing;
    private int id;
    private String registrationNumber;
    private CarType carType;

    public Car(CarType carType, String registNumber, String dateOfManufacturing, String imageSrc, double price) {
        this.carType = carType;
        this.registrationNumber = registNumber;
        this.dateOfManufacturing = dateOfManufacturing;
        this.image_src = imageSrc;
        this.basePricePerDay = price;
    }

    public Car() {

    }

    public Car(CarType carType, int carId, String registNumber, String dateOfManufacturing, double price) {
        this.carType = carType;
        this.id = carId;
        this.registrationNumber = registNumber;
        this.dateOfManufacturing = dateOfManufacturing;
        this.basePricePerDay = price;
    }


    public double getBasePricePerDay() {
        return basePricePerDay;
    }

    public String getDateOfManufacturing() {
        return dateOfManufacturing;
    }


    public int getId() {
        return id;
    }


    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public CarType getCarType() {
        return carType;
    }

    public String getImage_src() {
        return image_src;
    }

    public void setImage_src(String image_src) {
        this.image_src = image_src;
    }

    public void setBasePricePerDay(Double basePricePerDay) {
        this.basePricePerDay = basePricePerDay;
    }

    public void setDateOfManufacturing(String dateOfManufacturing) {
        this.dateOfManufacturing = dateOfManufacturing;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }
}