package by.epam.trainings.task7.controller.command.implementation.admin;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.CustomerDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.email.Sender;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:55
 * To change this template use File | Settings | File Templates.
 */
public class CheckCarCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(CheckCarCommand.class);
    private static final String ERROR_PAGE = "path.page.error";
    private static final String MESSAGE = "message";
    private static final String PARAM_NAME_CAR_BROKEN_TAG = "car_condition";
    private static final String PARAM_NAME_EMAIL_MSG = "emailMsg";
    private static final String PARAM_NAME_BOOKING_NUMBER = "bookingNumber";
    private static final String PARAM_NAME_CUSTOMER_DESCRIPTION = "description";
    private static final String PARAM_NAME_ADMIN_EMAIL_PASS = "emailPassword";
    private static final String TRUST =  "trust";
    private static final String NOT_TRUST =  "not trust";
    private static final String ADMIN_EMAIL = "snoopka1995@gmail.com";
    private static final String MSG_SUBJECT = "ELcars";
    private static final String OPTION_INDEX_PAGE = "path.page.option_index";
    private static final String REGIST_CAR_PAGE = "path.page.regist_car";


    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        CustomerDAO customerDAO = DAOFactory.getCustomerDAO();
        String carBrokenTag = request.getParameter(PARAM_NAME_CAR_BROKEN_TAG);
        String emailMessage = request.getParameter(PARAM_NAME_EMAIL_MSG);
        int bookingNumber = Integer.parseInt(request.getParameter(PARAM_NAME_BOOKING_NUMBER));
        String description = request.getParameter(PARAM_NAME_CUSTOMER_DESCRIPTION);
        String emailPassword = (String) request.getSession().getAttribute(PARAM_NAME_ADMIN_EMAIL_PASS);
        String login = null;
        String clientEmail = null;
        try {
            login = customerDAO.getLoginByBookingNumber(bookingNumber);
            clientEmail = customerDAO.getEmailByLogin(login);
            bookingDAO.delete(bookingNumber);
            if (carBrokenTag == null)
                customerDAO.setType(login,TRUST);
            else {
                customerDAO.setType(login, NOT_TRUST);
                customerDAO.setDescription(login, description);
            }
            if (emailMessage != null) {
                Sender tlsSender = new Sender(ADMIN_EMAIL, emailPassword);
                tlsSender.send(MSG_SUBJECT, emailMessage, ADMIN_EMAIL, clientEmail);
            }
            page = ConfigurationManager.getProperty(OPTION_INDEX_PAGE);
        } catch (DAOException e) {
            LOGGER.error("Error during car check: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
