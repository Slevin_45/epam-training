package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:53
 * To change this template use File | Settings | File Templates.
 */
public class GetAllCarsCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(GetAllCarsCommand.class);
    private static final String MESSAGE = "message";
    private static final String CARS = "cars";
    private static final String ROLE = "role";
    private static final String USER_ROLE = "user";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String CARS_PAGE = "path.page.cars";
    private static final String OPTION_CARS_PAGE = "path.page.option_cars";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Car> carList = null;
        CarDAO carDao = DAOFactory.getCarDAO();
        try {
            carList = carDao.findAll();
            request.setAttribute(CARS, carList);
            if (request.getSession().getAttribute(ROLE) == null || request.getSession().getAttribute(ROLE).equals(USER_ROLE))
                page = ConfigurationManager.getProperty(CARS_PAGE);
            else
                page = ConfigurationManager.getProperty(OPTION_CARS_PAGE);
        } catch (DAOException e) {
            LOGGER.error("Error during get all cars: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
