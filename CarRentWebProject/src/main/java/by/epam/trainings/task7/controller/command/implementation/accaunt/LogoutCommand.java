package by.epam.trainings.task7.controller.command.implementation.accaunt;

import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
public class LogoutCommand implements ICommand {
    private static final String INDEX_PAGE = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        HttpSession session = request.getSession(false);
        if (session != null)
            session.invalidate();
        page = ConfigurationManager.getProperty(INDEX_PAGE);
        return page;
    }
}
