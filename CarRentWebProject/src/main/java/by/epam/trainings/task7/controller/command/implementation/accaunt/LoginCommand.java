package by.epam.trainings.task7.controller.command.implementation.accaunt;

import by.epam.trainings.task7.beans.Customer;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 16.02.16
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
public class LoginCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";
    private static final String MESSAGE = "message";
    private static final String USER = "user";
    private static final String ROLE = "role";
    private static final String ADMIN_ROLE = "admin";
    private static final String EMAIL_PASSWORD = "emailPassword";
    private static final String USER_ID = "userId";
    private static final String USER_ROLE = "user";
    private static final String OPTION_INDEX_PAGE = "path.page.option_index";
    private static final String INDEX_PAGE = "path.page.index";
    private static final String LOGIN_PAGE = "path.page.login";
    private static final String ERROR_PAGE = "path.page.error";


    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String password = request.getParameter(PARAM_NAME_PASSWORD);
        Customer customer = null;
        try {
            customer = DAOFactory.getCustomerDAO().findCustomer(login, password);
            if (customer == null) {
                request.setAttribute(MESSAGE, MessageManager.INCORRECT_LOGIN_OR_EMAIL);
                page = ConfigurationManager.getProperty(LOGIN_PAGE);
            } else {
                if (customer.getRole().equals(ADMIN_ROLE)) {
                    request.getSession().setAttribute(USER, customer.getLogin());
                    request.getSession().setAttribute(ROLE, ADMIN_ROLE);
                    request.getSession().setAttribute(EMAIL_PASSWORD, password);
                    page = ConfigurationManager.getProperty(OPTION_INDEX_PAGE);
                } else {
                    request.getSession().setAttribute(USER, customer.getLogin());
                    request.getSession().setAttribute(USER_ID, customer.getId());
                    request.getSession().setAttribute(ROLE, USER_ROLE);
                    page = ConfigurationManager.getProperty(INDEX_PAGE);
                }
            }
        } catch (DAOException e) {
            LOGGER.error("Error during user login: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }

        return page;
    }

}
