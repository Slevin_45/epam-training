package by.epam.trainings.task7.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 03.01.16
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
public class LoginFilter implements Filter {
    private static final String USER_ROLE = "user";
    private static final String LOGIN_PAGE = "path.page.login";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (((HttpServletRequest) servletRequest).getSession().getAttribute(USER_ROLE) != null) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            ((HttpServletResponse) servletResponse).sendRedirect("/pages/login.jsp");
        }
    }

    @Override
    public void destroy() {
    }
}
