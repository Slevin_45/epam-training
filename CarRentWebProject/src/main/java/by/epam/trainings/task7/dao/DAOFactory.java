package by.epam.trainings.task7.dao;

import by.epam.trainings.task7.dao.impl.BookingDAOImpl;
import by.epam.trainings.task7.dao.impl.CarDAOImpl;
import by.epam.trainings.task7.dao.impl.CustomerDAOImpl;

public class DAOFactory {
    private static CustomerDAOImpl customerDAO = new CustomerDAOImpl();
    private static CarDAOImpl carDAO = new CarDAOImpl();
    private static BookingDAOImpl bookingDAO = new BookingDAOImpl();

    public static CustomerDAOImpl getCustomerDAO() {
        return customerDAO;
    }

    public static CarDAOImpl getCarDAO() {
        return carDAO;
    }

    public static BookingDAOImpl getBookingDAO() {
        return bookingDAO;
    }
}
