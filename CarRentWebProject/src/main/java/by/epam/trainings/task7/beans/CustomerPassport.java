package by.epam.trainings.task7.beans;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 01.01.16
 * Time: 23:08
 * To change this template use File | Settings | File Templates.
 */

public class CustomerPassport {

    private String number;
    private int id;
    private String date_of_issue;
    private String pass_expiry;
    private Customer customer;

    public CustomerPassport(Customer customer, String date_of_issue, String pass_number, String pass_expiry) {
        this.customer = customer;
        this.date_of_issue = date_of_issue;
        this.number = pass_number;
        this.pass_expiry = pass_expiry;
    }
    public String getNumber() {
        return number;
    }

    public String getDate_of_issue() {
        return date_of_issue;
    }


    public int getId() {
        return id;
    }

    public String getPass_expiry() {
        return pass_expiry;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setDate_of_issue(String date_of_issue) {
        this.date_of_issue = date_of_issue;
    }

    public void setPass_expiry(String pass_expiry) {
        this.pass_expiry = pass_expiry;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
