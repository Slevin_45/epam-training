package by.epam.trainings.task7.connectionFactory;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 03.01.16
 * Time: 20:55
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionFactory {
    private InitialContext ic;
    private DataSource ds;
    private final static Logger LOGGER = Logger.getLogger(ConnectionFactory.class);

    public Connection getConnection() throws ConnectionFactoryException {
        try {
            ic = new InitialContext();
            ds = (DataSource) ic.lookup("java:/comp/env/jdbc/car_rent");
            return ds.getConnection();
        } catch (SQLException | NamingException e) {
            throw new ConnectionFactoryException(e);
            //throw new ConnectionFactoryException(e);
          //  LOGGER.error("DataBase connection error :", e);
        }
    }
}
