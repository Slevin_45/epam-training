package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.beans.Customer;
import by.epam.trainings.task7.beans.CustomerPassport;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.CustomerDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:51
 * To change this template use File | Settings | File Templates.
 */
public class CreateBookingCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(CreateBookingCommand.class);
    private static final String PARAM_NAME_BOOKING_DATE = "booking_date";
    private static final String PARAM_NAME_RETURN_DATE = "return_date";
    private static final String PARAM_NAME_PASS_NUMBER = "pass_number";
    private static final String PARAM_NAME_DATE_ISSUE = "date_of_issue";
    private static final String PARAM_NAME_PASS_EXPIRY = "pass_expiry";
    private static final String PARAM_NAME_CAR_ID = "carId";
    private static final String PARAM_NAME_USER_ID = "userId";
    private static final String MESSAGE = "message";
    private static final String BOOKINGS = "bookings";
    private static final String BOOKINGS_PAGE = "path.page.bookings";
    private static final String ERROR_PAGE = "path.page.error";


    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        List<Booking> bookingList = null;
        String booking_date = request.getParameter(PARAM_NAME_BOOKING_DATE);
        String return_date = request.getParameter(PARAM_NAME_RETURN_DATE);
        String pass_number = request.getParameter(PARAM_NAME_PASS_NUMBER);
        String date_of_issue = request.getParameter(PARAM_NAME_DATE_ISSUE);
        String pass_expiry = request.getParameter(PARAM_NAME_PASS_EXPIRY);
        int carId = (int) request.getSession().getAttribute(PARAM_NAME_CAR_ID);
        int customerId = (int) request.getSession().getAttribute(PARAM_NAME_USER_ID);
        BookingDAO bookingDAO = DAOFactory.getBookingDAO();
        CustomerDAO customerDAO = DAOFactory.getCustomerDAO();
        try {
            if (bookingDAO.checkDate(carId, booking_date, return_date)) {
                Customer customer = new Customer(customerId);
                CustomerPassport customerPassport = new CustomerPassport(customer, date_of_issue, pass_number, pass_expiry);
                Booking booking = new Booking(customerPassport, carId, booking_date, return_date);
                bookingDAO.create(booking);
                bookingList = bookingDAO.getAllForUser(customerId);
                request.setAttribute(BOOKINGS, bookingList);
                if (customerDAO.checkTypeOfCustomer(customerId)) {
                    request.setAttribute(MESSAGE, MessageManager.BOOKING_SUCCESS);
                    page = ConfigurationManager.getProperty(BOOKINGS_PAGE);
                } else {
                    request.setAttribute(MESSAGE, MessageManager.BOOKING_SUCCESS_BUT_WAIT);
                    page = ConfigurationManager.getProperty(BOOKINGS_PAGE);
                }
            } else {
                request.setAttribute(MESSAGE, MessageManager.INCORRECT_DATE);
                page = ConfigurationManager.getProperty(BOOKINGS_PAGE);
            }
        } catch (DAOException e) {
            LOGGER.error("Error during create booking: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
