package by.epam.trainings.task7.controller.command.implementation;

import by.epam.trainings.task7.beans.Booking;
import by.epam.trainings.task7.controller.command.ICommand;
import by.epam.trainings.task7.dao.BookingDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import by.epam.trainings.task7.manager.ConfigurationManager;
import by.epam.trainings.task7.manager.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Loki
 * Date: 17.02.16
 * Time: 0:52
 * To change this template use File | Settings | File Templates.
 */
public class UpdateBookingCommand implements ICommand {
    private static final Logger LOGGER = Logger.getLogger(UpdateBookingCommand.class);
    private static final String BOOKINGS = "bookings";
    private static final String MESSAGE = "message";
    private static final String PARAM_NAME_USER_ID = "userId";
    private static final String PARAM_NAME_CAR_ID = "carId";
    private static final String PARAM_NAME_BOOKING_DATE = "bookingDate";
    private static final String PARAM_NAME_RETURN_DATE = "returnDate";
    private static final String PARAM_NAME_BOOKING_NUMBER = "bookingNumber";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String BOOKINGS_PAGE = "path.page.bookings";


    @Override
    public String execute(HttpServletRequest request) {
        List<Booking> bookingList = null;
        String page = null;
        Booking booking = null;
        BookingDAO bookingDao = DAOFactory.getBookingDAO();
        int userId = (int) request.getSession().getAttribute(PARAM_NAME_USER_ID);
        String bookingDate = request.getParameter(PARAM_NAME_BOOKING_DATE);
        String returnDate = request.getParameter(PARAM_NAME_RETURN_DATE);
        int bookingNumber = Integer.parseInt(request.getParameter(PARAM_NAME_BOOKING_NUMBER));
        int carId = Integer.parseInt(request.getParameter(PARAM_NAME_CAR_ID));
        try {
            if (bookingDao.checkDate(carId, bookingDate, returnDate)) {
                booking = new Booking();
                booking.setBookingDate(bookingDate);
                booking.setReturnDate(returnDate);
                booking.setBookingNumber(bookingNumber);
                bookingDao.update(booking);
                bookingList = bookingDao.getAllForUser(userId);
                request.setAttribute(BOOKINGS, bookingList);
                request.setAttribute(MESSAGE, MessageManager.BOOKING_UPDATE_SUCCESS);
                page = ConfigurationManager.getProperty(BOOKINGS_PAGE);

            } else {
                bookingList = bookingDao.getAllForUser(userId);
                request.setAttribute(BOOKINGS, bookingList);
                request.setAttribute(MESSAGE, MessageManager.BOOKING_UPDATE_ERROR);
                page = ConfigurationManager.getProperty(BOOKINGS_PAGE);

            }
        } catch (DAOException e) {
            LOGGER.error("Error during update booking: " + e);
            request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
            page = ConfigurationManager.getProperty(ERROR_PAGE);
        }
        return page;
    }
}
