<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html, body {
        height: 100%;
    }
    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%,#dddddd 31%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff8ef', endColorstr='#dddddd',GradientType=0 ); /* IE6-9 */
    }
 </style>
<jsp:include page="/pages/header.jsp" />
<div class="container">
    <c:if test="${not empty message }">
        <h4>
            <fmt:message key="${message}"/>
        </h4>
    </c:if>
    <table class="table table-hover">
        <thead>
        <tr>
            <th></th>
            <th><fmt:message key="booking.info.bookingNumber"/></th>
            <th><fmt:message key="booking.info.bookingDate"/></th>
            <th><fmt:message key="booking.info.returnDate"/></th>
            <th><fmt:message key="booking.info.status"/></th>
            <th><fmt:message key="car.info.carId"/></th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="booking" items="${bookings}">
            <tr>
                <form name="deleteForm" action="/secured/controller" id="deleteForm">
                    <td>
                        <button type="submit" class="btn btn-danger">
                            <input type="hidden" name="command" value="delete_booking"/>
                            <input type="hidden" name="bookingNumber" value="${booking.bookingNumber}"/>

                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        </form>
                <form name="editForm" action="/secured/controller" id="editForm" >
                    </td>
                    <td>${booking.bookingNumber}
                        <input type="hidden" name="bookingNumber" id="bookingNumber" value="${booking.bookingNumber}">
                    </td>
                    <td><input type="date" name="bookingDate" id="bookingDate"
                               value= ${booking.bookingDate}></td>
                    <td><input type="date" name="returnDate" id="returnDate"  value= ${booking.returnDate}>
                    </td>
                    <td>${booking.status}</td>
                    <td>${booking.carId}</td>
                    <input type="hidden" name="carId" type="text" value="${booking.carId}">
                    <td>
                        <c:if test="${booking.status == 'Checking'}">
                        <button type="submit" style=" margin-top: 0px;" class="btn btn-warning">
                            <input type="hidden" name="command" value="update_booking"/>
                            <input type="hidden" name="bookingNumber" value="${booking.bookingNumber}"/>
                            <input type="hidden" name="carId" value="${booking.carId}"/>
                            <fmt:message key="booking.button.edit"/>
                        </button>
                        </c:if>
                        </td>
                            <%--<input type="hidden" name="bookingNumber" value="${booking.bookingNumber}"/>--%>
                            <%--<input type="hidden" name="carId" value="${booking.carId}"/>--%>
                        <%--<c:if test="${booking.status == 'Checking'}">--%>
                            <%--<input type="button" value="<fmt:message key="booking.button.edit"/>" id="editButton"--%>
                                   <%--onclick="changeButton();" data-index = "${step}">--%>
                            <%--<input type="hidden" name="command" value="update_booking"/>--%>
                            <%--<input type="hidden" name="bookingNumber" value="${booking.bookingNumber}"/>--%>
                            <%--<input type="hidden" name="carId" value="${booking.carId}"/>--%>
                            <%--<script>--%>
                                <%--var smartButton = document.getElementById("editButton");--%>
                                <%--var myForm = document.getElementById("editForm");--%>
                                <%--function changeButton(e) {--%>
                                    <%--var smartButton = e.target,--%>
                                        <%--index = e.target.dataset.index;--%>
                                    <%--console.log(smartButton, index);--%>
                                    <%--if (smartButton.value == "<fmt:message key="booking.button.edit"/>") {--%>
                                        <%--document.getElementById("bookingDate").disabled = false;--%>
                                        <%--document.getElementById("returnDate").disabled = false;--%>
                                        <%--smartButton.value = "<fmt:message key="car.button.submit"/>";--%>
                                    <%--}--%>
                                    <%--else {--%>
                                        <%--myForm.action = '/secured/controller';--%>
                                        <%--myForm.submit();--%>
                                        <%--document.getElementById("bookingDate").disabled = true;--%>
                                        <%--document.getElementById("returnDate").disabled = true;--%>
                                        <%--smartButton.value = "<fmt:message key="booking.button.edit"/>";--%>
                                    <%--}--%>
                                <%--}--%>
                            <%--</script>--%>
                        <%--</c:if>--%>
                    </form>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
