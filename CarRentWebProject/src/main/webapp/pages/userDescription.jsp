<%--
  Created by IntelliJ IDEA.
  User: Loki
  Date: 15.02.16
  Time: 20:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="description" value="${sessionScope.description}" scope="session" />
<html>
<head>
    <title>User description</title>
</head>
<jsp:include page="/pages/option_header.jsp"/>
<style>
    html, body {
        height: 100%;
    }

    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff8ef', endColorstr='#dddddd', GradientType=0); /* IE6-9 */
    }
</style>
<body>
<div class="form-group">
    <label for="description">Description:</label>
    <textarea class="form-control" rows="5" id="description">${description}</textarea>
</div>
</body>
</html>