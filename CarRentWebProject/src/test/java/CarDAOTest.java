import by.epam.trainings.task7.beans.Car;
import by.epam.trainings.task7.beans.CarType;
import by.epam.trainings.task7.dao.CarDAO;
import by.epam.trainings.task7.dao.DAOException;
import by.epam.trainings.task7.dao.DAOFactory;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class CarDAOTest {
    private static final Logger LOGGER = Logger.getLogger(CarDAOTest.class);
    private static final String DATASOURCE_PATH  = "java:/comp/env/jdbc/car_rent";
    private CarDAO carDAO;

    @BeforeClass
    public static void setUpClass() throws Exception {
        try {
            System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,
                    "org.apache.naming");
            InitialContext ic = new InitialContext();
            ic.createSubcontext("java:");
            ic.createSubcontext("java:/comp");
            ic.createSubcontext("java:/comp/env");
            ic.createSubcontext("java:/comp/env/jdbc");
            MysqlDataSource ds = new MysqlDataSource();
            ds.setURL("jdbc:mysql://localhost/car_rent");
            ds.setUser("root");
            ds.setPassword("fotboll1995");
            ic.bind("java:/comp/env/jdbc/car_rent", ds);
        } catch (NamingException ex) {
            //Logger.getLogger(MyDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


    @Test
    public void testFindCar() throws DAOException, NamingException {
        carDAO = DAOFactory.getCarDAO();
        try {
            Car car = carDAO.find(6);
            int t = car.getId();
            Assert.assertEquals("e70A", car.getRegistrationNumber());
            Assert.assertEquals("2013-12-12", car.getDateOfManufacturing());
            Assert.assertEquals(100000.0, car.getBasePricePerDay(), 0.1);
            Assert.assertEquals(6, car.getId(), 0);
            Assert.assertEquals("POLO.png", car.getImage_src());
        } catch (DAOException e) {
            LOGGER.error(e);
        }
    }

    public void testCarCreate(){
        carDAO = DAOFactory.getCarDAO();
        Car car = new Car();
        CarType carType = new CarType();
        carType.setModel("m6");
        carType.setProducer("bmw");
        carType.setTransmission("auto");
        car.setCarType(carType);
        car.setBasePricePerDay(100.0);
        car.setRegistrationNumber("e800");
        car.setImage_src("Pajero.png");
        car.setDateOfManufacturing("2014-12-12");
        try {
            carDAO.create(car);
            int id = car.getId();
            Assert.assertNotNull(id);
            Car newCar = carDAO.find(id);
            Assert.assertEquals("e70A", car.getRegistrationNumber());
            Assert.assertEquals("2013-12-12", car.getDateOfManufacturing());
            Assert.assertEquals(100000.0, car.getBasePricePerDay(), 0.1);
            Assert.assertEquals(6, car.getId(), 0);
            Assert.assertEquals("POLO.png", car.getImage_src());
            Assert.assertEquals("m6", car.getCarType().getModel());
            Assert.assertEquals("bmw", car.getCarType().getProducer());
            Assert.assertEquals("auto", car.getCarType().getTransmission());
            carDAO.delete(id);

        } catch (DAOException e) {
            LOGGER.error(e);
        }
    }

}
