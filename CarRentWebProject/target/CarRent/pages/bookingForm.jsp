<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${sessionScope.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="text"/>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>BookingForm</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<style>
    html, body {
        height: 100%;
    }

    body {
        font-family: sans-serif;

        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fff8ef+0,dddddd+31,dddddd+100 */
        background: #fff8ef; /* Old browsers */
        background: -moz-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom, #fff8ef 0%, #dddddd 31%, #dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff8ef', endColorstr='#dddddd', GradientType=0); /* IE6-9 */
    }
</style>
<jsp:include page="/pages/header.jsp"/>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form action="/secured/controller" method="post">
                        <input type="hidden" name="command" value="create_booking"/>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><fmt:message
                                            key="booking.info.bookingDate"/></label>
                                    <input type="date" name="booking_date" id="booking_date"
                                           class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><fmt:message
                                            key="booking.info.returnDate"/></label>
                                    <input type="date" name="return_date" id="return_date"
                                           class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="pass_number" id="pass_number" class="form-control input-sm"
                                   placeholder="<fmt:message key="customer.info.passportN"/>">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><fmt:message key="customer.info.dateOfIssue"/></label>
                            <input type="date" name="date_of_issue" id="date_of_issue"
                                   class="form-control input-sm">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><fmt:message key="customer.info.PassExpiry"/></label>
                            <input type="date" name="pass_expiry" id="pass_expiry"
                                   class="form-control input-sm">
                        </div>
                        <input type="submit" value="Pay" class="btn btn-info btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>