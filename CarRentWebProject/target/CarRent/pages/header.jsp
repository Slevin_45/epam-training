<%--
  Created by IntelliJ IDEA.
  User: Loki
  Date: 07.02.16
  Time: 15:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="/WEB-INF/tld/custom.tld" prefix="customTag"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${sessionScope.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        .navbar {
            margin-bottom: 0;
        }
        .lang_ch {
            position: absolute;
            top: 60px;
            left: 5px;
            z-index: 200;
        }
        .lang_ch a {
            color: #9D9D9D;
            border: 1px solid #9d9d9d;
            padding: 3px
        }
        .lang_ch a:hover {
            color: #fff;
            border-color: #fff;
        }
        .btn{
            margin-top: 9px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/pages/index.jsp"><customTag:logo /></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/pages/index.jsp"><fmt:message key="navbar.href.home"/></a></li>
                <li>
                    <form action="/controller" method="post">
                        <input type="hidden" name="command" value="show_cars"/>
                        <button type="submit" class="btn btn-link"><fmt:message key="navbar.href.cars"/></button>
                    </form>
                </li>
                <li>
                    <form action="/secured/controller" method="post">
                        <input type="hidden" name="command" value="show_bookings"/>
                        <button type="submit" class="btn btn-link"><fmt:message key="navbar.href.bookings"/></button>
                    </form>
                </li>
                <li><a href="tel:+375296361723" class="phone">+375 (29) 290-09-08</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/pages/login.jsp"><span class="glyphicon glyphicon-log-in"></span><fmt:message key="navbar.href.login"/></a></li>
                <li>
                    <form action="/controller" method="post">
                        <input type="hidden" name="command" value="logout"/>
                        <button type="submit" class="btn btn-link"><span class="glyphicon glyphicon-log-out"></span><fmt:message key="navbar.href.logout"/></button>
                    </form>

                </li>
            </ul>
        </div>
    </div>
</nav>
</body>
</html>